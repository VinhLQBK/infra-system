﻿using System.Threading.Tasks;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.IService
{
    public interface IServiceChargePackageMappingService
    {
        Task<ServiceChargePackageMapping> AddServiceChargePackageMapping(ServiceChargePackageMapping serviceChargePackageMapping);
    }
}
