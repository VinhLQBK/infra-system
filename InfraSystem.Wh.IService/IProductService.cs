﻿using System.Threading.Tasks;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.IService
{
    public interface IProductService
    {
        Task<Product> AddProduct(Product product);
    }
}
