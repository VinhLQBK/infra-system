﻿using System.Threading.Tasks;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.IService
{
    public interface IPackageService
    {
        Task<Package> AddPackage(Package package);
        Task<Package> GetByTrackingCode(string trackingCode);
        Task<Package> GetLeast(string warehouseCode);
    }
}
