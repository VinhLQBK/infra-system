﻿using InfraSystem.Shared.EF.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace InfraSystem.Shared.EF.Implement
{
    public abstract class Entity<T> : IEntity
    {
        [Key] public T Id { get; set; }
    }
}
