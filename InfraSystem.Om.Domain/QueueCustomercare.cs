﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

#nullable disable

namespace InfraSystem.Om.Domain
{
    public partial class QueueCustomercare
    {
        public int Id { get; set; }
        public string AccountId { get; set; }
        public string CareBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}