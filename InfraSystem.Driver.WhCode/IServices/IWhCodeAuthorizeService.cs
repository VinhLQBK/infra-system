﻿using System.Threading.Tasks;

namespace InfraSystem.Driver.WhCode.IServices
{
    public interface IWhCodeAuthorizeService
    {
          Task<string> GetAuthorizeToken();
    }
}
