﻿using System;
using System.Threading.Tasks;

using InfraSystem.Driver.WhCode.Common;
using InfraSystem.Driver.WhCode.IServices;
using InfraSystem.Driver.WhCode.Models.Requests;
using InfraSystem.Driver.WhCode.Models.Response;
using Microsoft.Extensions.Options;

namespace InfraSystem.Driver.WhCode.Services
{
    public class WhCodeService : BaseService, IWhCodeService
    {
        public WhCodeService(
            IWhCodeHttpClient httpClient, 
            IWhCodeAuthorizeService authorizeservice, 
            // IHttpContextAccessor httpContextAccessor, 
            IOptions<WhCodeApiEndPoints> options) 
            : base(httpClient, authorizeservice, options)
        {
        }

        public async  Task GenShipmentCodeAsync(GenShipmentCodeRequest model)
        {
            try
            {
                var res = await base.PostAsync(_config.WhCode.GenShipmentCode,model);
            }
            catch (Exception ex)
            {
                throw;
            }
            return;
        }

        public async Task<ShipmentCodeUseStatusResponse> GetShipmentCodeStatusAsync(ShipmentCodeUseStatusRequest model)
        {
            var res = await base.PostAsync<BaseResponse<ShipmentCodeUseStatusResponse>,ShipmentCodeUseStatusRequest>(_config.WhCode.GetShipmentCodeStatus,model);
            return res?.data;
        }
    }
}
