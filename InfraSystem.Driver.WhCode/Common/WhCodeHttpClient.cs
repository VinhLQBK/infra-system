﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Wrap;

namespace InfraSystem.Driver.WhCode
{
    #region 00. NUGET Package : Poly , Logging , Json.NET

    #endregion

    #region 00. AppSetting

    /// <summary>
    /// Auth info from header name
    /// </summary>
    public static class IChibaHeaderNames
    {
        public static readonly string LanguageId = "IChiba-Language-Id";
        public static readonly string PostOfficeId = "IChiba-PostOffice-Id";
        public static readonly string UserId = "IChiba-User-Id";
        public static readonly string UserName = "IChiba-User-Name";
    }
    /// <summary>
    /// WhCode Api Constant
    /// </summary>
    public class WhCodeApiConstant
    {
        public const string ConfigurationKey = "WhCodeApiEndPoints";
    }


    /// <summary>
    /// Api Config
    /// </summary>
    public class WhCodeApiEndPoints
    {
        private WhCode _whCode = new WhCode();

        public WhCode WhCode { get => _whCode; set => _whCode = value; }
    }

    public class WhCode
    {
        private string _GenShipmentCode = "api/ShipmentCode/GenShipmentCode";
        private string _GetShipmentCodeStatus = "api/ShipmentCode/GetShipmentCodeStatus";

        public string GetShipmentCodeStatus
        {
            get { return _GetShipmentCodeStatus; }
            set { _GetShipmentCodeStatus = value; }
        }

        public string GenShipmentCode
        {
            get { return _GenShipmentCode; }
            set { _GenShipmentCode = value; }
        }

    }

    #endregion

    #region 01. Common
    public static class Serialize
    {
        public static string JsonSerializeObject<T>(T obj)
        {
            if (obj == null)
                return string.Empty;
            return JsonConvert.SerializeObject(
                value: obj,
                formatting: Formatting.None,
                settings: new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
        }

    }
    #endregion

    #region 02. IHttpClient  Interface
    public interface IWhCodeHttpClient
    {
        Task<string> GetStringAsync(
          string uri,
          string authorizationToken = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null);

        Task<HttpResponseMessage> PostAsync<T>(
          string uri,
          T item,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null);

        Task<HttpResponseMessage> PostAsync(
          string uri,
          MultipartFormDataContent content,
          Dictionary<string, string> headers = null);

        Task<HttpResponseMessage> PostAsync(string uri, string content, Dictionary<string, string> headers = null);

        Task<HttpResponseMessage> DeleteAsync(
          string uri,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null);

        Task<HttpResponseMessage> PutAsync<T>(
          string uri,
          T item,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null);
    }
    #endregion

    #region 03. IHttpClient Implementation interface
    public class WhCodeHttpClientConnection
    {
        private static volatile WhCodeHttpClientConnection _instance;
        public static readonly object SyncLock = new object();
        private HttpClient _httpClient;

        public static WhCodeHttpClientConnection Current
        {
            get
            {
                if (WhCodeHttpClientConnection._instance == null)
                {
                    lock (WhCodeHttpClientConnection.SyncLock)
                    {
                        if (WhCodeHttpClientConnection._instance == null)
                            WhCodeHttpClientConnection._instance = new WhCodeHttpClientConnection();
                    }
                }
                return WhCodeHttpClientConnection._instance;
            }
        }

        private WhCodeHttpClientConnection() => this._httpClient = this.Create();

        public HttpClient GetConnection => this._httpClient ?? (this._httpClient = this.Create());

        private HttpClient Create()
        {
            HttpClient httpClient = new HttpClient() { };
            httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");
            return httpClient;
        }
    }
    public class WhCodeHttpClient : IWhCodeHttpClient
    {
        private readonly HttpClient _client;
        private readonly ILogger<WhCodeHttpClient> _logger;
        private readonly string _baseUrl;
        private readonly Func<string, IEnumerable<IAsyncPolicy>> _policyCreator;
        private readonly ConcurrentDictionary<string, AsyncPolicyWrap> _policyWrappers;

        public WhCodeHttpClient(
          Func<string, IEnumerable<AsyncPolicy>> policyCreator,
          ILogger<WhCodeHttpClient> logger,
          string baseUrl = null)
        {
            this._client = WhCodeHttpClientConnection.Current.GetConnection;
            this._logger = logger;
            this._policyCreator = policyCreator;
            this._policyWrappers = new ConcurrentDictionary<string, AsyncPolicyWrap>();

            if (!string.IsNullOrWhiteSpace(baseUrl))
            {
                this._client.BaseAddress = new Uri(baseUrl);
            }
        }

        public Task<HttpResponseMessage> PostAsync<T>(
          string uri,
          T item,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            return this.DoPostPutAsync<T>(HttpMethod.Post, uri, item, authorizationToken, requestId, authorizationMethod, headers);
        }

        public Task<HttpResponseMessage> PostAsync(
          string uri,
          MultipartFormDataContent content,
          Dictionary<string, string> headers = null)
        {
            return this.DoPostPutAsync(HttpMethod.Post, uri, content, headers);
        }

        public Task<HttpResponseMessage> PostAsync(string uri, string content, Dictionary<string, string> headers = null)
            => this.DoPostPutAsync(HttpMethod.Post, uri, content, headers);

        public Task<HttpResponseMessage> PutAsync<T>(
          string uri,
          T item,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            return this.DoPostPutAsync<T>(HttpMethod.Put, uri, item, authorizationToken, requestId, authorizationMethod, headers);
        }

        public Task<HttpResponseMessage> DeleteAsync(
          string uri,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            return this.HttpInvoker<HttpResponseMessage>(GetOrigin(uri), async () =>
            {
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Delete, uri);
                if (!string.IsNullOrWhiteSpace(authorizationToken) && !string.IsNullOrWhiteSpace(authorizationMethod))
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue(authorizationMethod, authorizationToken);
                else if (!string.IsNullOrWhiteSpace(authorizationToken))
                    requestMessage.Headers.TryAddWithoutValidation("Authorization", authorizationToken);
                if (requestId != null)
                    requestMessage.Headers.Add("x-requestid", requestId);
                if (headers != null && headers.Keys.Count > 0)
                {
                    foreach (var key in headers.Keys)
                    {
                        requestMessage.Headers.Add(key, headers[key]);
                    }
                }
                HttpResponseMessage httpResponseMessage = await this._client.SendAsync(requestMessage);
                requestMessage = null;
                return httpResponseMessage;
            });
        }

        private string GetOrigin(string uri)
        {
            return this._client.BaseAddress != null ? WhCodeHttpClient.GetOriginFromUri(this._client.BaseAddress) : WhCodeHttpClient.GetOriginFromUri(uri);
        }

        public Task<string> GetStringAsync(
          string uri,
          string authorizationToken = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            return this.HttpInvoker<string>(GetOrigin(uri), async () =>
           {
               HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, uri);
               if (!string.IsNullOrWhiteSpace(authorizationToken) && !string.IsNullOrWhiteSpace(authorizationMethod))
                   requestMessage.Headers.Authorization = new AuthenticationHeaderValue(authorizationMethod, authorizationToken);
               else if (!string.IsNullOrWhiteSpace(authorizationToken))
                   requestMessage.Headers.TryAddWithoutValidation("Authorization", authorizationToken);
               if (headers != null && headers.Keys.Count > 0)
               {
                   foreach (var key in headers.Keys)
                   {
                       requestMessage.Headers.Add(key, headers[key]);
                   }
               }
               HttpResponseMessage response = await this._client.SendAsync(requestMessage);
               if (response.StatusCode == HttpStatusCode.InternalServerError)
                   throw new HttpRequestException();
               string str = await response.Content.ReadAsStringAsync();
               requestMessage = null;
               response = null;
               return str;
           });
        }

        private Task<HttpResponseMessage> DoPostPutAsync(
          HttpMethod method,
          string uri,
          string item,
          Dictionary<string, string> headers = null)
        {
            if (method != HttpMethod.Post && method != HttpMethod.Put)
                throw new ArgumentException("Value must be either post or put.", nameof(method));
            return this.HttpInvoker<HttpResponseMessage>(GetOrigin(uri), async () =>
            {
                HttpRequestMessage requestMessage = new HttpRequestMessage(method, uri)
                {
                    Content = new StringContent(item, Encoding.UTF8, "application/json")
                };
                if (headers != null && headers.Keys.Count > 0)
                {
                    foreach (var key in headers.Keys)
                    {
                        requestMessage.Headers.Add(key, headers[key]);
                    }
                }
                HttpResponseMessage response = await this._client.SendAsync(requestMessage);
                HttpResponseMessage httpResponseMessage = response.StatusCode != HttpStatusCode.InternalServerError ? response : throw new HttpRequestException();
                requestMessage = null;
                response = null;
                return httpResponseMessage;
            });
        }

        private Task<HttpResponseMessage> DoPostPutAsync<T>(
          HttpMethod method,
          string uri,
          T item,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            if (method != HttpMethod.Post && method != HttpMethod.Put)
                throw new ArgumentException("Value must be either post or put.", nameof(method));
            return this.HttpInvoker<HttpResponseMessage>(GetOrigin(uri), async () =>
            {
                HttpRequestMessage requestMessage = new HttpRequestMessage(method, uri)
                {
                    Content = new StringContent(Serialize.JsonSerializeObject<T>(item), Encoding.UTF8, "application/json")
                };
                if (!string.IsNullOrWhiteSpace(authorizationToken) && !string.IsNullOrWhiteSpace(authorizationMethod))
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue(authorizationMethod, authorizationToken);
                else if (!string.IsNullOrWhiteSpace(authorizationToken))
                    requestMessage.Headers.TryAddWithoutValidation("Authorization", authorizationToken);
                if (requestId != null)
                    requestMessage.Headers.Add("x-requestid", requestId);
                if (headers != null && headers.Keys.Count > 0)
                {
                    foreach (var key in headers.Keys)
                    {
                        requestMessage.Headers.Add(key, headers[key]);
                    }
                }
                HttpResponseMessage response = await this._client.SendAsync(requestMessage);
                HttpResponseMessage httpResponseMessage = response.StatusCode != HttpStatusCode.InternalServerError ? response : throw new HttpRequestException();
                requestMessage = null;
                response = null;
                return httpResponseMessage;
            });
        }

        private Task<HttpResponseMessage> DoPostPutAsync(
          HttpMethod method,
          string uri,
          MultipartFormDataContent content,
          Dictionary<string, string> headers = null)
        {
            if (method != HttpMethod.Post && method != HttpMethod.Put)
                throw new ArgumentException("Value must be either post or put.", nameof(method));
            return this.HttpInvoker<HttpResponseMessage>(GetOrigin(uri), async () =>
            {
                HttpRequestMessage requestMessage = new HttpRequestMessage(method, uri)
                {
                    Content = content
                };
                if (headers != null && headers.Keys.Count > 0)
                {
                    foreach (var key in headers.Keys)
                    {
                        requestMessage.Headers.Add(key, headers[key]);
                    }
                }
                HttpResponseMessage response = await this._client.SendAsync(requestMessage);
                HttpResponseMessage httpResponseMessage = response.StatusCode != HttpStatusCode.InternalServerError ? response : throw new HttpRequestException();
                requestMessage = null;
                response = null;
                return httpResponseMessage;
            });
        }

        private async Task<T> HttpInvoker<T>(string origin, Func<Task<T>> action)
        {
            string normalizedOrigin = WhCodeHttpClient.NormalizeOrigin(origin);
            AsyncPolicyWrap policyWrap;
            if (!this._policyWrappers.TryGetValue(normalizedOrigin, out policyWrap))
            {
                policyWrap = Policy.WrapAsync(this._policyCreator(normalizedOrigin).ToArray<IAsyncPolicy>());
                this._policyWrappers.TryAdd(normalizedOrigin, policyWrap);
            }
            T obj = await policyWrap.ExecuteAsync<T>(action);
            normalizedOrigin = null;
            policyWrap = null;
            return obj;
        }

        private static string NormalizeOrigin(string origin) => origin?.Trim()?.ToLower();

        private static string GetOriginFromUri(string uri)
        {
            Uri uri1 = new Uri(uri);
            return string.Format("{0}://{1}:{2}", uri1.Scheme, uri1.DnsSafeHost, uri1.Port);
        }
        private static string GetOriginFromUri(Uri uri)
        {
            return string.Format("{0}://{1}:{2}", uri.Scheme, uri.DnsSafeHost, uri.Port);
        }


    }
    public class WhCodeStdHttpClient : IWhCodeHttpClient
    {
        private readonly HttpClient _client;
        private ILogger<WhCodeStdHttpClient> _logger;

        public WhCodeStdHttpClient(ILogger<WhCodeStdHttpClient> logger)
        {
            this._client = WhCodeHttpClientConnection.Current.GetConnection;
            this._logger = logger;
        }

        public async Task<string> GetStringAsync(
          string uri,
          string authorizationToken = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, uri);
            if (authorizationToken != null)
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue(authorizationMethod, authorizationToken);
            if (headers != null && headers.Keys.Count > 0)
            {
                foreach (var key in headers.Keys)
                {
                    requestMessage.Headers.Add(key, headers[key]);
                }
            }
            HttpResponseMessage response = await this._client.SendAsync(requestMessage);
            string str = await response.Content.ReadAsStringAsync();
            requestMessage = null;
            response = null;
            return str;
        }

        private async Task<HttpResponseMessage> DoPostPutAsync(
          HttpMethod method,
          string uri,
          string item,
          Dictionary<string, string> headers = null)
        {
            if (method != HttpMethod.Post && method != HttpMethod.Put)
                throw new ArgumentException("Value must be either post or put.", nameof(method));
            HttpRequestMessage requestMessage = new HttpRequestMessage(method, uri);
            requestMessage.Content = new StringContent(Serialize.JsonSerializeObject<string>(item), Encoding.UTF8, "application/json");
            if (headers != null && headers.Keys.Count > 0)
            {
                foreach (var key in headers.Keys)
                {
                    requestMessage.Headers.Add(key, headers[key]);
                }
            }
            HttpResponseMessage response = await this._client.SendAsync(requestMessage);
            HttpResponseMessage httpResponseMessage = response.StatusCode != HttpStatusCode.InternalServerError ? response : throw new HttpRequestException();
            requestMessage = null;
            response = null;
            return httpResponseMessage;
        }

        private async Task<HttpResponseMessage> DoPostPutAsync<T>(
          HttpMethod method,
          string uri,
          T item,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            if (method != HttpMethod.Post && method != HttpMethod.Put)
                throw new ArgumentException("Value must be either post or put.", nameof(method));
            HttpRequestMessage requestMessage = new HttpRequestMessage(method, uri);
            requestMessage.Content = new StringContent(Serialize.JsonSerializeObject<T>(item), Encoding.UTF8, "application/json");
            if (authorizationToken != null)
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue(authorizationMethod, authorizationToken);
            if (requestId != null)
                requestMessage.Headers.Add("x-requestid", requestId);
            if (headers != null && headers.Keys.Count > 0)
            {
                foreach (var key in headers.Keys)
                {
                    requestMessage.Headers.Add(key, headers[key]);
                }
            }
            HttpResponseMessage response = await this._client.SendAsync(requestMessage);
            HttpResponseMessage httpResponseMessage = response.StatusCode != HttpStatusCode.InternalServerError ? response : throw new HttpRequestException();
            requestMessage = null;
            response = null;
            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> PostAsync<T>(
          string uri,
          T item,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            HttpResponseMessage httpResponseMessage = await this.DoPostPutAsync<T>(HttpMethod.Post, uri, item, authorizationToken, requestId, authorizationMethod, headers);
            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> PostAsync(
          string uri,
          MultipartFormDataContent content, Dictionary<string, string> headers = null)
        {
            throw new NotImplementedException();
        }

        public async Task<HttpResponseMessage> PostAsync(
          string uri,
          string content,
          Dictionary<string, string> headers = null)
        {
            HttpResponseMessage httpResponseMessage = await this.DoPostPutAsync(HttpMethod.Post, uri, content, headers);
            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> PutAsync<T>(
          string uri,
          T item,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            HttpResponseMessage httpResponseMessage = await this.DoPostPutAsync<T>(HttpMethod.Put, uri, item, authorizationToken, requestId, authorizationMethod, headers);
            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> DeleteAsync(
          string uri,
          string authorizationToken = null,
          string requestId = null,
          string authorizationMethod = "Bearer",
          Dictionary<string, string> headers = null)
        {
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Delete, uri);
            if (authorizationToken != null)
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue(authorizationMethod, authorizationToken);
            if (requestId != null)
                requestMessage.Headers.Add("x-requestid", requestId);
            if (headers != null && headers.Keys.Count > 0)
            {
                foreach (var key in headers.Keys)
                {
                    requestMessage.Headers.Add(key, headers[key]);
                }
            }
            HttpResponseMessage httpResponseMessage = await this._client.SendAsync(requestMessage);
            requestMessage = null;
            return httpResponseMessage;
        }


    }
    #endregion

    #region 04. Factory Pattern
    public interface IWhCodeHttpClientFactory
    {
        WhCodeHttpClient CreateResilientHttpClient();
    }
    public class WhCodeHttpClientFactory : IWhCodeHttpClientFactory
    {
        private readonly ILogger<WhCodeHttpClient> _logger;
        private readonly int _retryCount;
        private readonly string _baseUrl;
        private readonly int _exceptionsAllowedBeforeBreaking;

        public WhCodeHttpClientFactory(
          ILogger<WhCodeHttpClient> logger,
          int exceptionsAllowedBeforeBreaking = 5,
          int retryCount = 6,
          string baseUrl = null)
        {
            this._logger = logger;
            this._exceptionsAllowedBeforeBreaking = exceptionsAllowedBeforeBreaking;
            this._retryCount = retryCount;
            _baseUrl = baseUrl;
        }

        public WhCodeHttpClient CreateResilientHttpClient() => new WhCodeHttpClient(origin => this.CreatePolicies(), this._logger, _baseUrl);

        private AsyncPolicy[] CreatePolicies() => new AsyncPolicy[2]
        {
            Policy.Handle<HttpRequestException>()
                  .WaitAndRetryAsync(
                 this._retryCount,
                 retryAttempt => TimeSpan.FromSeconds(Math.Pow(2.0,  retryAttempt)),
                 (exception, timeSpan, retryCount, context) =>
                    {
                        string str = string.Format("Retry {0} implemented with Polly's RetryPolicy ",  retryCount) + "of " + context.PolicyKey + " at " + context.OperationKey + ", " + string.Format("due to: {0}.",  exception);
                        LoggerExtensions.LogWarning( _logger, str, Array.Empty<object>());
                        LoggerExtensions.LogDebug( _logger, str, Array.Empty<object>());
                    }
                 ),
            Policy.Handle<HttpRequestException>()
                  .CircuitBreakerAsync(
                        this._exceptionsAllowedBeforeBreaking,
                        TimeSpan.FromMinutes(1.0),
                        (exception, duration) => LoggerExtensions.LogTrace( _logger, "Circuit breaker opened", Array.Empty<object>()),
                        () => LoggerExtensions.LogTrace( _logger, "Circuit breaker reset", Array.Empty<object>())
                  )
        };
    }
    #endregion

    #region 05. Using (Register in NetCore DI)
    // Startup.cs 
    //services.AddSingleton<IResilientHttpClientFactory, ResilientHttpClientFactory>(sp =>
    //    {
    //        const int retryCount = 3;
    //        const int exceptionsAllowedBeforeBreaking = 5;
    //        return new ResilientHttpClientFactory(null, exceptionsAllowedBeforeBreaking, retryCount);
    //    });
    //services.AddSingleton<IHttpClient, ResilientHttpClient>(sp => sp.GetService<IResilientHttpClientFactory>().CreateResilientHttpClient());

    // Register API Client 
    //services.Configure<InfraSystemApiConfig>(Configuration.GetSection("InFraSystem:InfraSystemApiConfig"));
    //services.AddSingleton(sp => sp.GetRequiredService<IOptions<InfraSystemApiConfig>>().Value);
    //services.AddSingleton<Ichiba.InfraSystem.MQ.Driver.IAuthorizeClient, IChiba.Api.OP.Infrastructure.InfraSystemAuthorizeClientImplement>();
    //services.AddSingleton<InfraSystemApiClient>();

    #endregion
}
