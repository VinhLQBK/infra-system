﻿namespace InfraSystem.Application.WarehouseLocation
{
   public class WarehouseLocationResponse
    {
        public string  ReferenceNumber { get; set; }
        public string LocationName { get; set; }
    }
}
