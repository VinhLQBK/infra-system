﻿namespace InfraSystem.Application.WarehouseLocation.Queries.RequestDto
{
   public class QueryWarehouseLocationRequest
    {
        public string ReferenceNumber { get; set; }
        public decimal? WeightShipment { get; set; }
        public decimal? LengthShipment { get; set; }
        public decimal? HeightShipment { get; set; }
        public decimal? WidthShipment { get; set; }
        public string LocationName { get; set; }
    }
}
