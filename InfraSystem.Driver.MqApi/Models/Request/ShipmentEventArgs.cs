﻿using System;
using System.Collections.Generic;

namespace InfraSystem.Driver.MqApi.Request
{
    public class ShipmentEventArgs
    {
        /// <summary>
        /// Current shipment data
        /// </summary>
        public string Shipment { get; set; }

        /// <summary>
        /// Update Data
        /// </summary>
        public string ShipmentUpdate { get; set; }

        /// <summary>
        /// Loại sự kiện
        /// </summary>
        public string EventType { get; set; }

        public DateTime EventDateUtc { get; set; }

        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByUsername { get; set; }
    }

    public class Shipment
    {
        public Shipment()
        {
            //DispatchDetails = new HashSet<DispatchDetail>();
            //Payables = new HashSet<Payable>();
            //Payments = new HashSet<Payment>();
            //Receivables = new HashSet<Receivable>();
            ShipmentCargoAddServices = new HashSet<ShipmentCargoAddService>();
            ShipmentItems = new HashSet<ShipmentItem>();
            //ShipmentRefDestinationShipments = new HashSet<ShipmentRef>();
            //ShipmentRefSourceShipments = new HashSet<ShipmentRef>();
            SubShipments = new HashSet<SubShipment>();
        }

        public string Id { get; set; }
        public string ShipmentNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public string DispatchBagId { get; set; }
        public string DispatchBagNumber { get; set; }
        public string DispatchId { get; set; }
        public string HouseId { get; set; }
        public string MasterId { get; set; }
        public string ManifestId { get; set; }
        public string ManifestNo { get; set; }
        public bool? IsDirectShipment { get; set; }
        public string CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerContact { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerCountryId { get; set; }
        public string CustomerCountryText { get; set; }
        public string CustomerStateProvinceId { get; set; }
        public string CustomerStateProvinceText { get; set; }
        public string CustomerDistrictId { get; set; }
        public string CustomerDistrictText { get; set; }
        public string CustomerWardId { get; set; }
        public string CustomerWardText { get; set; }
        public string CustomerAddress1 { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerZipPostalCode { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string PickupName { get; set; }
        public string PickupEmail { get; set; }
        public string PickupCountryId { get; set; }
        public string PickupCountryText { get; set; }
        public string PickupStateProvinceId { get; set; }
        public string PickupStateProvinceText { get; set; }
        public string PickupDistrictId { get; set; }
        public string PickupDistrictText { get; set; }
        public string PickupWardId { get; set; }
        public string PickupWardText { get; set; }
        public string PickupAddress1 { get; set; }
        public string PickupAddress2 { get; set; }
        public string PickupZipPostalCode { get; set; }
        public string PickupPhoneNumber { get; set; }
        public string ShipperId { get; set; }
        public string ShipperCode { get; set; }
        public string ShipperName { get; set; }
        public string ShipperContact { get; set; }
        public string ShipperEmail { get; set; }
        public string ShipperCountryId { get; set; }
        public string ShipperCountryText { get; set; }
        public string ShipperStateProvinceId { get; set; }
        public string ShipperStateProvinceText { get; set; }
        public string ShipperDistrictId { get; set; }
        public string ShipperDistrictText { get; set; }
        public string ShipperWardId { get; set; }
        public string ShipperWardText { get; set; }
        public string ShipperAddress1 { get; set; }
        public string ShipperAddress2 { get; set; }
        public string ShipperZipPostalCode { get; set; }
        public string ShipperPhoneNumber { get; set; }
        public string ShipperFwdAgentId { get; set; }
        public string ConsigneeId { get; set; }
        public string ConsigneeCode { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsigneeContact { get; set; }
        public string ConsigneeEmail { get; set; }
        public string ConsigneeCountryId { get; set; }
        public string ConsigneeCountryText { get; set; }
        public string ConsigneeStateProvinceId { get; set; }
        public string ConsigneeStateProvinceText { get; set; }
        public string ConsigneeDistrictId { get; set; }
        public string ConsigneeDistrictText { get; set; }
        public string ConsigneeWardId { get; set; }
        public string ConsigneeWardText { get; set; }
        public string ConsigneeAddress1 { get; set; }
        public string ConsigneeAddress2 { get; set; }
        public string ConsigneeZipPostalCode { get; set; }
        public string ConsigneePhoneNumber { get; set; }
        public string ConsigneeFwdAgentId { get; set; }
        public int? CargoShippingMethod { get; set; }
        public string CargoShippingMethodText { get; set; }
        public int? CargoType { get; set; }
        public string CargoTypeText { get; set; }
        public bool Cod { get; set; }
        public decimal? Codamount { get; set; }
        public string IncotermId { get; set; }
        public string IncotermCode { get; set; }
        public string TruckerId { get; set; }
        public string TruckerCode { get; set; }
        public string PostOfficeToSendId { get; set; }
        public string PostOfficeToSendCode { get; set; }
        public string PostOfficeToReceiveId { get; set; }
        public string PostOfficeToReceiveCode { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public DateTime? ImportOnUtc { get; set; }
        public DateTime? ExportOnUtc { get; set; }
        public string CargoSpserviceId { get; set; }
        public string CargoSpserviceCode { get; set; }
        public string Note { get; set; }
        public int? ExpectedPieces { get; set; }
        public int? Pieces { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalDeclaredCustomsValue { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public decimal? VolumetricDivisor { get; set; }
        public decimal? TotalGrossWeight { get; set; }
        public decimal? TotalVolumetricWeight { get; set; }
        public decimal? TotalChargeableWeight { get; set; }
        public string CurrencyId { get; set; }
        public string CurrencyCode { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal? TotalAmountUsd { get; set; }
        public decimal? TotalDeclaredCustomsValueUsd { get; set; }
        public string MeasureDimensionId { get; set; }
        public string MeasureDimensionCode { get; set; }
        public string MeasureWeightId { get; set; }
        public string MeasureWeightCode { get; set; }
        public string LocationName { get; set; }
        public bool? IsProcessed { get; set; }
        public bool? IsSendEmail { get; set; }
        public string ProcessedBy { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public DateTime? CustomsClearanceUpdatedOnUtc { get; set; }
        public int? ReasonId { get; set; }
        public string ReasonText { get; set; }
        public int? SolutionId { get; set; }
        public string SolutionText { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByUserName { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedByUserName { get; set; }
        public DateTime? UpdatedOnUtc { get; set; }
        public bool Deleted { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedByUserName { get; set; }
        public DateTime? DeletedOnUtc { get; set; }
        public string ThirdPartyBillId { get; set; }
        public string ThirdPartyBillNo { get; set; }

        public virtual ICollection<ShipmentCargoAddService> ShipmentCargoAddServices { get; set; }
        public virtual ICollection<ShipmentItem> ShipmentItems { get; set; }
        public virtual ICollection<SubShipment> SubShipments { get; set; }

        //public virtual ICollection<ShipmentRef> ShipmentRefDestinationShipments { get; set; }
        //public virtual ICollection<ShipmentRef> ShipmentRefSourceShipments { get; set; }

    }
    //   public partial class ManifestDetail
    //{
    //    public string Id { get; set; }
    //    public string ManifestId { get; set; }
    //    public string DispatchId { get; set; }
    //    public int? Status { get; set; }
    //    public string StatusText { get; set; }

    //    public virtual Dispatch Dispatch { get; set; }
    //    public virtual Manifest Manifest { get; set; }
    //}
    // public partial class Dispatch
    //{
    //    public Dispatch()
    //    {
    //        DispatchBags = new HashSet<DispatchBag>();
    //        DispatchDetails = new HashSet<DispatchDetail>();
    //        ManifestDetails = new HashSet<ManifestDetail>();
    //    }

    //    public string Id { get; set; }
    //    public string MasterId { get; set; }
    //    public string HouseId { get; set; }
    //    public string DispatchNumber { get; set; }
    //    public int CargoShippingMethod { get; set; }
    //    public string CargoShippingMethodText { get; set; }
    //    public string CargoSpserviceId { get; set; }
    //    public string CargoSpserviceCode { get; set; }
    //    public string ReferenceTransportNumber { get; set; }
    //    public string FromCountryId { get; set; }
    //    public string FromCountryCode { get; set; }
    //    public string ToCountryId { get; set; }
    //    public string ToCountryCode { get; set; }
    //    public string FromPostOfficeId { get; set; }
    //    public string FromPostOfficeCode { get; set; }
    //    public string ToPostOfficeId { get; set; }
    //    public string ToPostOfficeCode { get; set; }
    //    public int Type { get; set; }
    //    public string TypeText { get; set; }
    //    public string SppartnerId { get; set; }
    //    public string SppartnerCode { get; set; }
    //    public int? SppartnerType { get; set; }
    //    public string SppartnerTypeText { get; set; }
    //    public string FromPortId { get; set; }
    //    public string FromPortCode { get; set; }
    //    public string ToPortId { get; set; }
    //    public string ToPortCode { get; set; }
    //    public string Mawb { get; set; }
    //    public string Hawb { get; set; }
    //    public int TotalBags { get; set; }
    //    public int TotalShipments { get; set; }
    //    public decimal TotalGrossWeight { get; set; }
    //    public decimal TotalVolumetricWeight { get; set; }
    //    public decimal TotalChargeableWeight { get; set; }
    //    public int Status { get; set; }
    //    public string StatusText { get; set; }
    //    public int ReceptionState { get; set; }
    //    public string ReceptionStateText { get; set; }
    //    public string CommodityId { get; set; }
    //    public string CommodityCode { get; set; }
    //    public string CommodityGroupId { get; set; }
    //    public string CommodityGroupCode { get; set; }
    //    public string CustomerId { get; set; }
    //    public string CustomerCode { get; set; }
    //    public decimal? MaxWeightshipment { get; set; }
    //    public DateTime? FromShipmentProcessDate { get; set; }
    //    public DateTime? ToShipmentProcessDate { get; set; }
    //    public string Note { get; set; }
    //    public string CreatedBy { get; set; }
    //    public string CreatedByUserName { get; set; }
    //    public DateTime CreatedOnUtc { get; set; }
    //    public string UpdatedBy { get; set; }
    //    public string UpdatedByUserName { get; set; }
    //    public DateTime? UpdatedOnUtc { get; set; }
    //    public bool Deleted { get; set; }
    //    public string DeletedBy { get; set; }
    //    public string DeletedByUserName { get; set; }
    //    public DateTime? DeletedOnUtc { get; set; }

    //    public virtual ICollection<DispatchBag> DispatchBags { get; set; }
    //    public virtual ICollection<DispatchDetail> DispatchDetails { get; set; }
    //    public virtual ICollection<ManifestDetail> ManifestDetails { get; set; }
    //}
    //public partial class DispatchDetail
    //{
    //    public string Id { get; set; }
    //    public string DispatchBagId { get; set; }
    //    public string DispatchId { get; set; }
    //    public string ShipmentId { get; set; }
    //    public string CreatedBy { get; set; }
    //    public string CreatedByUserName { get; set; }
    //    public DateTime CreatedOnUtc { get; set; }

    //    public virtual Dispatch Dispatch { get; set; }
    //    public virtual DispatchBag DispatchBag { get; set; }
    //    public virtual Shipment Shipment { get; set; }
    //}
    //public partial class DispatchBag
    //{
    //    public DispatchBag()
    //    {
    //        DispatchDetails = new HashSet<DispatchDetail>();
    //        Shipments = new HashSet<Shipment>();
    //    }

    //    public string Id { get; set; }
    //    public string DispatchId { get; set; }
    //    public string HouseId { get; set; }
    //    public string MasterId { get; set; }
    //    public string DispatchBagNumber { get; set; }
    //    public string DispatchBagName { get; set; }
    //    public int? BagOrder { get; set; }
    //    public string BagTypeId { get; set; }
    //    public string BagTypeCode { get; set; }
    //    public int TotalShipments { get; set; }
    //    public decimal TotalGrossWeight { get; set; }
    //    public decimal TotalVolumetricWeight { get; set; }
    //    public decimal TotalChargeableWeight { get; set; }
    //    public string LocationName { get; set; }
    //    public string LocationCode { get; set; }
    //    public int? CargoShippingMethod { get; set; }
    //    public string CargoShippingMethodText { get; set; }
    //    public string CargoSpserviceId { get; set; }
    //    public string CargoSpserviceCode { get; set; }
    //    public string ReferenceTransportNumber { get; set; }
    //    public string FromCountryId { get; set; }
    //    public string FromCountryCode { get; set; }
    //    public string ToCountryId { get; set; }
    //    public string ToCountryCode { get; set; }
    //    public string FromPostOfficeId { get; set; }
    //    public string FromPostOfficeCode { get; set; }
    //    public string ToPostOfficeId { get; set; }
    //    public string ToPostOfficeCode { get; set; }
    //    public int? Type { get; set; }
    //    public string TypeText { get; set; }
    //    public string SppartnerId { get; set; }
    //    public string SppartnerCode { get; set; }
    //    public int? SppartnerType { get; set; }
    //    public string SppartnerTypeText { get; set; }
    //    public string FromPortId { get; set; }
    //    public string FromPortCode { get; set; }
    //    public string ToPortId { get; set; }
    //    public string ToPortCode { get; set; }
    //    public int ReceptionState { get; set; }
    //    public string ReceptionStateText { get; set; }
    //    public string CommodityId { get; set; }
    //    public string CommodityCode { get; set; }
    //    public string CommodityGroupId { get; set; }
    //    public string CommodityGroupCode { get; set; }
    //    public string CustomerId { get; set; }
    //    public string CustomerCode { get; set; }
    //    public decimal? MaxWeightShipment { get; set; }
    //    public DateTime? FromShipmentProcessDate { get; set; }
    //    public DateTime? ToShipmentProcessDate { get; set; }
    //    public bool IsLastBag { get; set; }
    //    public int Status { get; set; }
    //    public string StatusText { get; set; }
    //    public string Note { get; set; }
    //    public string CreatedBy { get; set; }
    //    public string CreatedByUserName { get; set; }
    //    public DateTime CreatedOnUtc { get; set; }
    //    public string UpdatedBy { get; set; }
    //    public string UpdatedByUserName { get; set; }
    //    public DateTime? UpdatedOnUtc { get; set; }
    //    public bool Deleted { get; set; }
    //    public string DeletedBy { get; set; }
    //    public string DeletedByUserName { get; set; }
    //    public DateTime? DeletedOnUtc { get; set; }

    //    public virtual Dispatch Dispatch { get; set; }
    //    public virtual HouseBill House { get; set; }
    //    public virtual MasterBill Master { get; set; }
    //    public virtual ICollection<DispatchDetail> DispatchDetails { get; set; }
    //    public virtual ICollection<Shipment> Shipments { get; set; }
    //}

    //public partial class Manifest
    //{
    //    public Manifest()
    //    {
    //        ManifestDetails = new HashSet<ManifestDetail>();
    //        Shipments = new HashSet<Shipment>();
    //    }

    //    public string Id { get; set; }
    //    public string ManifestNo { get; set; }
    //    public int CargoShippingMethod { get; set; }
    //    public string CargoShippingMethodText { get; set; }
    //    public string CargoSpserviceId { get; set; }
    //    public string CargoSpserviceCode { get; set; }
    //    public string ExportCompanyId { get; set; }
    //    public string ExportCompanyCode { get; set; }
    //    public int TotalShipments { get; set; }
    //    public decimal TotalGrossWeight { get; set; }
    //    public decimal TotalVolumetricWeight { get; set; }
    //    public decimal TotalChargeableWeight { get; set; }
    //    public decimal TotalDeclaredCustomsValue { get; set; }
    //    public decimal? TotalDeclaredCustomsValueUsd { get; set; }
    //    public string Note { get; set; }
    //    public int? Status { get; set; }
    //    public string StatusText { get; set; }
    //    public string CreatedBy { get; set; }
    //    public string CreatedByUserName { get; set; }
    //    public DateTime CreatedOnUtc { get; set; }
    //    public string UpdatedBy { get; set; }
    //    public string UpdatedByUserName { get; set; }
    //    public DateTime? UpdatedOnUtc { get; set; }
    //    public bool Deleted { get; set; }
    //    public string DeletedBy { get; set; }
    //    public string DeletedByUserName { get; set; }
    //    public DateTime? DeletedOnUtc { get; set; }
    //    public string Mawb { get; set; }
    //    public string Hawb { get; set; }
    //    public string PostOfficeId { get; set; }
    //    public string SppartnerId { get; set; }
    //    public string SppartnerCode { get; set; }
    //    public int? SppartnerType { get; set; }
    //    public string SppartnerTypeText { get; set; }
    //    public string SppartnerName { get; set; }
    //    public string BookingId { get; set; }

    //    public virtual Booking Booking { get; set; }
    //    public virtual ICollection<ManifestDetail> ManifestDetails { get; set; }
    //    public virtual ICollection<Shipment> Shipments { get; set; }
    //}

    public partial class ShipmentCargoAddService
    {
        public string Id { get; set; }
        public string ShipmentId { get; set; }
        public string ShipmentNumber { get; set; }
        public string CargoAddServiceId { get; set; }
        public string CargoAddServiceCode { get; set; }
        public bool? IsProcessed { get; set; }
        public bool? IsSendEmail { get; set; }
        public string ProcessedBy { get; set; }
        public string SendEmailBy { get; set; }
        public DateTime? ProcessedOnUtc { get; set; }
        public DateTime? SendEmailOnUtc { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByUserName { get; set; }
        public DateTime? CreatedOnUtc { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedByUserName { get; set; }
        public DateTime? UpdatedOnUtc { get; set; }
        public bool? Deleted { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedByUserName { get; set; }
        public DateTime? DeletedOnUtc { get; set; }

        public virtual Shipment Shipment { get; set; }
    }

    public partial class ShipmentItem
    {
        public string Id { get; set; }
        public string ShipmentId { get; set; }
        public string CommodityId { get; set; }
        public string CommodityCode { get; set; }
        public string CommodityText { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Hscode { get; set; }
        public string Sku { get; set; }
        public string CommodityGroupId { get; set; }
        public string CommodityGroupCode { get; set; }
        public string CountryOfOriginId { get; set; }
        public string CountryOfOriginCode { get; set; }
        public string CountryOfOriginText { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public decimal? VolumetricDivisor { get; set; }
        public decimal? VolumetricWeight { get; set; }
        public decimal TotalGrossWeight { get; set; }
        public decimal Quantity { get; set; }
        public string QuantityUnitId { get; set; }
        public string QuantityUnitCode { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal DeclaredCustomsValue { get; set; }
        public decimal TotalDeclaredCustomsValue { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal? PriceUsd { get; set; }
        public decimal? TotalAmountUsd { get; set; }
        public decimal? DeclaredCustomsValueUsd { get; set; }
        public decimal? TotalDeclaredCustomsValueUsd { get; set; }
        public string CurrencyId { get; set; }
        public string CurrencyCode { get; set; }
        public string MeasureDimensionId { get; set; }
        public string MeasureDimensionCode { get; set; }
        public string MeasureWeightId { get; set; }
        public string MeasureWeightCode { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }

        public virtual Shipment Shipment { get; set; }
    }

    public partial class SubShipment
    {
        public string Id { get; set; }
        public string ShipmentId { get; set; }
        public string ReferenceNumber { get; set; }
        public string SubShipmentNumber { get; set; }
        public string WarehouseLocationId { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public decimal? TotalGrossWeight { get; set; }
        public decimal? VolumetricWeight { get; set; }
        public decimal? TotalChargeableWeight { get; set; }
        public string LocationName { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string OrderNumber { get; set; }

        public virtual Shipment Shipment { get; set; }
    }
    //public partial class ShipmentRef
    //{
    //    public ShipmentRef()
    //    {
    //        ShipmentRefItems = new HashSet<ShipmentRefItem>();
    //    }

    //    public string Id { get; set; }
    //    public string SourceOrderId { get; set; }
    //    public string SourceShipmentId { get; set; }
    //    public string DestinationShipmentId { get; set; }
    //    public string ReferenceNumber { get; set; }
    //    public string GroupName { get; set; }
    //    public int Type { get; set; }
    //    public string TypeText { get; set; }
    //    public int Method { get; set; }
    //    public string MethodText { get; set; }
    //    public int? ShipmentProcessType { get; set; }
    //    public string ShipmentProcessTypeText { get; set; }
    //    public int? SplitCondition { get; set; }
    //    public string SplitConditionText { get; set; }
    //    public int? MergeCondition { get; set; }
    //    public string MergeConditionText { get; set; }
    //    public string CreatedBy { get; set; }
    //    public string CreatedByUserName { get; set; }
    //    public DateTime CreatedOnUtc { get; set; }

    //    public virtual Shipment DestinationShipment { get; set; }
    //    public virtual Sporder SourceOrder { get; set; }
    //    public virtual Shipment SourceShipment { get; set; }
    //    public virtual ICollection<ShipmentRefItem> ShipmentRefItems { get; set; }
    //}

    //public partial class ShipmentRefItem
    //{
    //    public string Id { get; set; }
    //    public string ShipmentRefId { get; set; }
    //    public string SourceOrderDetailId { get; set; }
    //    public string SourceShipmentDetailId { get; set; }
    //    public string DestinationShipmentDetailId { get; set; }
    //    public decimal? Quantity { get; set; }

    //    public virtual ShipmentRef ShipmentRef { get; set; }
    //}

    //public partial class Payable
    //{
    //    public string Id { get; set; }
    //    public string ShipmentId { get; set; }
    //    public string ChargeTypeId { get; set; }
    //    public string ChargeTypeCode { get; set; }
    //    public string ChargeTypeName { get; set; }
    //    public decimal? Quantity { get; set; }
    //    public string QuantityUnit { get; set; }
    //    public decimal? Price { get; set; }
    //    public string CurrencyId { get; set; }
    //    public decimal? Rate { get; set; }
    //    public decimal? Amount { get; set; }
    //    public string Note { get; set; }
    //    public string CreatedById { get; set; }
    //    public DateTime? CreatedDateUtc { get; set; }
    //    public string ModifiedById { get; set; }
    //    public DateTime? ModifiedDateUtc { get; set; }
    //    public bool? IsDeleted { get; set; }
    //    public string DeletedById { get; set; }
    //    public DateTime? DeletedDateUtc { get; set; }

    //    public virtual Shipment Shipment { get; set; }
    //}

    //public partial class Payment
    //{
    //    public string Id { get; set; }
    //    public string ShipmentId { get; set; }
    //    public string PaymentMethodId { get; set; }
    //    public decimal? Amount { get; set; }
    //    public int? Status { get; set; }
    //    public string Note { get; set; }
    //    public string CreatedById { get; set; }
    //    public DateTime? CreatedDateUtc { get; set; }
    //    public string ModifiedById { get; set; }
    //    public DateTime? ModifiedDateUtc { get; set; }
    //    public bool? IsDeleted { get; set; }
    //    public string DeletedById { get; set; }
    //    public DateTime? DeletedDateUtc { get; set; }

    //    public virtual Shipment Shipment { get; set; }
    //}
    //public partial class Receivable
    //{
    //    public string Id { get; set; }
    //    public string ShipmentId { get; set; }
    //    public string ChargeTypeId { get; set; }
    //    public string ChargeTypeCode { get; set; }
    //    public string ChargeTypeName { get; set; }
    //    public decimal? Quantity { get; set; }
    //    public string QuantityUnit { get; set; }
    //    public decimal? Price { get; set; }
    //    public string CurrencyId { get; set; }
    //    public decimal? Rate { get; set; }
    //    public decimal? Amount { get; set; }
    //    public string Note { get; set; }
    //    public string CreatedById { get; set; }
    //    public DateTime? CreatedDateUtc { get; set; }
    //    public string ModifiedById { get; set; }
    //    public DateTime? ModifiedDateUtc { get; set; }
    //    public bool? IsDeleted { get; set; }
    //    public string DeletedById { get; set; }
    //    public DateTime? DeletedDateUtc { get; set; }

    //    public virtual Shipment Shipment { get; set; }
    //}
}
