﻿namespace InfraSystem.Driver.MqApi.Request
{
    public class CheckNewProductCodeRequestModel
    {
        public string PostOfficeId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string CommodityCode { get; set; }
        public string  CountryCode { get; set; }
    }
}
