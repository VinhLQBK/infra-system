﻿namespace InfraSystem.Driver.MqApi.Request
{
    public class WarehouseLocationCacheRequest
    {
        public string PostOfficeId { get; set; }
        public string CustomerCode { get; set; }
    }
}
