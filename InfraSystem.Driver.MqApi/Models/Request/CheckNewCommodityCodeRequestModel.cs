﻿namespace InfraSystem.Driver.MqApi.Request
{
    public class CheckNewCommodityCodeRequestModel
    { 

        public string CommodityCode { get; set; }
        public string CommodityText { get; set; }
        
    }
}
