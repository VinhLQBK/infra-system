﻿//using System;
//using InfraSystem.Driver.MqApi.Request;
//using Microsoft.Extensions.Options;
//using Newtonsoft.Json;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using System.Web;
//using InfraSystem.Driver.MqApi.Request;

//namespace InfraSystem.Driver.MqApi
//{
//    public class MQApiConfig
//    {

//        public string BaseUrl { get; set; }
//        public string AutoSearchWarehouseLocation { get; set; }
//        public string calculatorWarehouseLocation { get; set; }
//        public string GetByPostOfficeId { get; set; }
//    }
//    public static class MQApiEnpointConfig
//    {
//        public const string MASTER_CHECK_COMMODITY_CODE = "/api/commodity/master_commodity_sync_commodity_code";

//        public const string MASTER_CHECK_PRODUCT_CODE = "/api/product/master_product_sync_product_code";

//        public const string OP_SHIPMENT_CREATE = "/api/shipment/create";
//        public const string OP_SHIPMENT_CREATES = "/api/shipment/creates";
//        public const string OP_SHIPMENT_TRANSLATE_ITEM = "/api/Shipment/translate-item";

//        public const string OP_SHIPMENT_EVENT = "/api/event-log/shipment-event";
//        public const string OP_SHIPMENT_LOG_PARTNER_REQUEST = "/api/event-log/partner-event";

//        public const string calculatorWarehouseLocation = "/api/WarehouseLocation/cache-warehouseLocation";
//        public const string CacheWarehouseLocationByCustomerCode = "/api/WarehouseLocation/cache-warehouseLocation-by-customerCode";
//    }
//    public class MQApiClient : BaseClient
//    {
//        private readonly MQApiConfig _mqApiConfig;

//        public MQApiClient(IHttpClient httpClient,
//            IAuthorizeClient authorizeClient,
//            IOptions<MQApiConfig> mqOptions)
//            : base(httpClient, authorizeClient)
//        {
//            _mqApiConfig = mqOptions.Value;
//        }

//        #region Warehouse Location cache

//        public async Task<BaseResponse> calculatorWarehouseLocation(CalculateLocationMessage request)
//        {
//            // var response = await PostAsync<BaseResponse, CalculateLocationMessage>(_mqApiConfig.calculatorWarehouseLocation, request);
//            var response = await PostAsync<BaseResponse, CalculateLocationMessage>(_mqApiConfig.BaseUrl + MQApiEnpointConfig.calculatorWarehouseLocation, request);

//            return response;
//        }

//        #endregion

//        #region Master DB Queue 
//        /// <summary>
//        ///  Queue check new product code and add to Master DB
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns></returns>
//        public async Task<bool> CheckNewProductCodeAsync(List<CheckNewProductCodeRequestModel> request)
//        {
//            var res = await PostAsync(_mqApiConfig.BaseUrl + MQApiEnpointConfig.MASTER_CHECK_PRODUCT_CODE, request);
//            return res.IsSuccessStatusCode;
//        }

//        #endregion

//        #region OP  Service 


//        /// <summary>
//        /// Log Shipment Event
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns></returns>
//        public async Task<bool> ShipmentEvent(ShipmentEventArgs request)
//        {
//            var res = await PostAsync(_mqApiConfig.BaseUrl + MQApiEnpointConfig.OP_SHIPMENT_EVENT, request);
//            return res.IsSuccessStatusCode;
//        }

//        /// <summary>
//        /// Log Shipment Event
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns></returns>
//        public async Task<bool> ShipmentEvent(string request)
//        {
//            var data = JsonConvert.DeserializeObject<ShipmentEventArgs>(request);
//            var res = await PostAsync(_mqApiConfig.BaseUrl + MQApiEnpointConfig.OP_SHIPMENT_EVENT, data);
//            // return  true; 
//            return res.IsSuccessStatusCode;
//        }

//        /// <summary>
//        /// Partner Event
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns></returns>
//        public async Task<bool> PublishPartnerEventAsync(PartnerEventModel req)
//        {
//            try
//            {
//                var res = await PostAsync(_mqApiConfig.BaseUrl + MQApiEnpointConfig.OP_SHIPMENT_LOG_PARTNER_REQUEST, req);
//                // return  true; 
//                return res.IsSuccessStatusCode;
//            }
//            catch (Exception)
//            {

//                // throw;
//            }
//            return false;
//        }


//        public async Task<BaseResponse> CacheByCustomerCode(WarehouseLocationCacheRequest request)
//        {
//            var response = await PostAsync<BaseResponse, WarehouseLocationCacheRequest>(_mqApiConfig.BaseUrl + MQApiEnpointConfig.CacheWarehouseLocationByCustomerCode, request);
//            return response;
//        }

//        public async Task<bool> CheckNewCommodityCodeAsync(List<CheckNewCommodityCodeRequestModel> data)
//        {
//            var res = await PostAsync(_mqApiConfig.BaseUrl + MQApiEnpointConfig.MASTER_CHECK_COMMODITY_CODE, data);
//            return true;
//        }

//        /// <summary>
//        /// Translate Shipment Item text
//        /// </summary>
//        /// <param name="shipmentId"></param>
//        /// <returns></returns>
//        public async Task TranslateShipmentItemAsync(string shipmentId)
//        {
//            await PostAsync(_mqApiConfig.BaseUrl + MQApiEnpointConfig.OP_SHIPMENT_TRANSLATE_ITEM + $"/{HttpUtility.UrlEncode(shipmentId)}", "");
//            return;
//        }



//        /// <summary>
//        /// Translate Shipment Item text
//        /// </summary>
//        /// <param name="shipmentId"></param>
//        /// <returns></returns>
//        public async Task<bool> CreateShipment(ShipmentCreateModel model)
//        {
//            var res = await PostAsync($"{_mqApiConfig.BaseUrl}{MQApiEnpointConfig.OP_SHIPMENT_CREATE}", model);
//            return res.IsSuccessStatusCode;
//        }

//        /// <summary>
//        /// Translate Shipment Item text
//        /// </summary>
//        /// <param name="shipmentId"></param>
//        /// <returns></returns>
//        public async Task<bool> CreateShipments(List<ShipmentCreateModel> models)
//        {
//            var res = await PostAsync($"{_mqApiConfig.BaseUrl}{MQApiEnpointConfig.OP_SHIPMENT_CREATES}", models);
//            return res.IsSuccessStatusCode;
//        }

//        #endregion
//    }
//}
