﻿using InfraSystem.Driver.MqApi.IServices;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Polly;

using RestEase.HttpClientFactory;

using System;

namespace InfraSystem.Driver.MqApi.Extensions
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddMqApiService(this IServiceCollection services, IConfiguration configuration)
        {
            //Config
            var hostingConfig = configuration.GetValue<string>("Hosting:ApisConfig:MqApi");

            services.AddMqApiService<IEventLogMqApi>(hostingConfig);
            services.AddMqApiService<IProductMqApi>(hostingConfig);
            services.AddMqApiService<IShipmentMqApi>(hostingConfig);

            return services;
        }


        private static IServiceCollection AddMqApiService<T>(this IServiceCollection services, string baseUrl) where T : class
        {

            services.AddRestEaseClient<T>(baseUrl)
                // .AddHttpMessageHandler<MasterPrivateWebWorkContextDelegatingHandler>()
                .AddTransientHttpErrorPolicy(builder =>
                    builder.WaitAndRetryAsync(new[]
                    {
                        TimeSpan.FromMilliseconds(100),
                        TimeSpan.FromMilliseconds(500),
                        TimeSpan.FromSeconds(2)
                    }));
            return services;
        }

    }
}
