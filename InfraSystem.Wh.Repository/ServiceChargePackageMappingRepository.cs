﻿using InfraSystem.Wh.IRepository;
using Core.Repository.Abstract;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.Repository
{
    public class ServiceChargePackageMappingRepository : BaseRepository<WarehouseDbContext, ServiceChargePackageMapping>, IServiceChargePackageMappingRepository
    {
        public ServiceChargePackageMappingRepository(WarehouseDbContext dbContext) : base(dbContext)
        {
        }
    }
}
