﻿using InfraSystem.Wh.IRepository;
using Core.Repository.Abstract;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.Repository
{
    public class CountryRepository : BaseRepository<WarehouseDbContext, Country>, ICountryRepository
    {
        public CountryRepository(WarehouseDbContext dbContext) : base(dbContext)
        {
        }
    }
}
