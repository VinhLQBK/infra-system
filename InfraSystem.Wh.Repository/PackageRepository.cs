﻿using InfraSystem.Wh.IRepository;
using Core.Repository.Abstract;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.Repository
{
    public class PackageRepository: BaseRepository<WarehouseDbContext, Package>, IPackageRepository
    {
        public PackageRepository(WarehouseDbContext dbContext) : base(dbContext)
        {
        }
    }
}
