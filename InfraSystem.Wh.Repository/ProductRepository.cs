﻿using InfraSystem.Wh.IRepository;
using Core.Repository.Abstract;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.Repository
{
    public class ProductRepository : BaseRepository<WarehouseDbContext, Product>, IProductRepository
    {
        public ProductRepository(WarehouseDbContext dbContext) : base(dbContext)
        {
        }
    }
}
