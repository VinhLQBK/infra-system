﻿using InfraSystem.Wh.IRepository;
using Core.Repository.Abstract;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.Repository
{
    public class CategoryRepository : BaseRepository<WarehouseDbContext, Category>, ICategoryRepository
    {
        public CategoryRepository(WarehouseDbContext dbContext) : base(dbContext)
        {
        }
    }
}
