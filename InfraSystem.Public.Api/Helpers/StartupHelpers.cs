﻿using InfraSystem.Cache.Interface;
using InfraSystem.Cache.Redis.Implement;
using InfraSystem.Cache.Redis.Implement.Config;
using InfraSystem.Private.Api.AutoMappers;
using InfraSystem.Shared.Common;
using InfraSystem.Shared.Constants;
using InfraSystem.Shared.Extensions;
using InfraSystem.Shared.RedisCache.Implements;
using InfraSystem.Shared.RedisCache.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using InfraSystem.Wh.Domain;
using Assembly = InfraSystem.Application.Assembly;

namespace InfraSystem.Private.Api.Helpers
{
    public static class StartupHelpers
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<WarehouseDbContext>(opt =>
            {
                opt.UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString(ConfigurationKeys.MasterConnectionString));
            });
            return services;
        }

        public static IServiceCollection AddMediatREvent(this IServiceCollection services)
        {
            services.AddMediatR(typeof(Assembly).GetTypeInfo().Assembly);
            return services;
        }

        public static IServiceCollection AddRedisCache(this IServiceCollection services, IConfiguration configuration)
        {
            ConfigurationRoot configurationRoot = (ConfigurationRoot)configuration;
            IEnumerable<Microsoft.Extensions.Configuration.IConfigurationProvider>
                configurationProviders = configurationRoot.Providers
                    .Where(p => p.GetType() == typeof(JsonConfigurationProvider)
                                && ((JsonConfigurationProvider)p).Source.Path.StartsWith("appsettings"));

            foreach (Microsoft.Extensions.Configuration.IConfigurationProvider item in configurationProviders)
            {
                ConfigSetting.Init<RedisConnectionConfig>(item);
            }

            #region Redis config

            services.AddSingleton(configuration.GetSection("Redis").Get<InfraSystemRedisConfig>());
            services.AddSingleton<RedisConnection<InfraSystemRedisConfig>>();
            services.AddTransient<IRedisStorage<InfraSystemRedisConfig>, RedisStorage<InfraSystemRedisConfig>>();

            services.AddTransient<IRedisStorage, RedisStorage<InfraSystemRedisConfig>>();

            #endregion Redis config

            services.AddTransient<IWarehouseLocationCache, WarehouseLocationCache>();
            services.AddTransient<IWarehouseLocationCacheCalculator, WarehouseLocationCacheCalculator>();
            services.AddTransient<IWarehouseLocationCachePostOfficeId, WarehouseLocationCachePostOfficeId>();
            services.AddTransient<IWarehouseLocationCacheUnknow, WarehouseLocationCacheUnknow>();

            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            var mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingToWarehouseLocationCacheByLocationName());

            });
            services.AddSingleton(mapperConfig.CreateMapper().RegisterMap());
            return services;
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "InfraSystem Private Api v1",
                    Version = "v1"
                });
            });
        }

        public static void UseSwaggerUi(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "InfraSystem private api v1");
                c.DocumentTitle = "InfraSystem private api v1";
            });
        }
    }
}
