using System;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace InfraSystem.Private.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
             .Enrich.FromLogContext()
             .WriteTo.Console(outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff z}][{Level:u3}]{NewLine}    {Message:lj}{NewLine}{Exception}{NewLine}")
             .WriteTo.File(
                 path: "logs" + System.IO.Path.DirectorySeparatorChar + "log-{Date}.txt",
                 outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff z}][{Level:u3}]{NewLine}    {Message:lj}{NewLine}{Exception}{NewLine}",
                 rollingInterval: RollingInterval.Day,
                 rollOnFileSizeLimit: true)
             .CreateLogger();

            // CreateHostBuilder(args).Build().Run();
            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
            
            }
            finally
            {
             
            }
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseSerilog()
            .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
