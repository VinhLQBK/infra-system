﻿namespace InfraSystem.Private.Api.Request
{
    public class WarehouseLocationShipmentDetails
    {
        public string ReferenceNumber { get; set; }
        public int? WeightShipment { get; set; }
        public int? LengthShipment { get; set; }
        public int? HeightShipment { get; set; }
        public int? WidthShipment { get; set; }
        public string LocationName { get; set; }
    }
}
