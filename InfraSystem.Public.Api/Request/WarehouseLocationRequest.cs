﻿using System.Collections.Generic;

namespace InfraSystem.Private.Api.Request
{
    public class WarehouseLocationRequest
    {
        public string CustomerCode { get; set; }
        public string CargoShippingMethod { get; set; }
        public string CargoSPService { get; set; }
        public string CargoType { get; set; }
        public string PostOfficeId { get; set; }
        public string ShipmentNumber { get; set; }
        public int? Pieces { get; set; }
        public List<WarehouseLocationShipmentDetails> shipmentDetails { get; set; }
        public WarehouseLocationRequest()
        {
            shipmentDetails = new List<WarehouseLocationShipmentDetails>();
        }
    }
}
