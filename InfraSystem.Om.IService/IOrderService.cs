﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InfraSystem.Om.Domain;

namespace InfraSystem.Om.IService
{
    public interface IOrderService
    {
        Task<Order> GetByShipmentCode(string shipmentCode);
        Task<List<Order>> GetByTrackingCode(string trackingCode);
    }
}
