﻿using InfraSystem.Cache.Interface;
using InfraSystem.Shared.RedisCache.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Redis.Implement
{
    public class BaseCache<TEntity, TId> : IBaseCache<TEntity, TId>
    {
        protected readonly IRedisStorage redisStorage;
        protected readonly string key;

        public BaseCache(IRedisStorage redisStorage,
            string key)
        {
            this.redisStorage = redisStorage;
            this.key = key;
        }

        protected string BuildParameter(params object[] parameters)
        {
            var separator = string.Empty;
            var result = string.Empty;

            foreach (var item in parameters)
            {
                if (item == null || string.IsNullOrWhiteSpace(item.ToString()))
                {
                    continue;
                }

                var value = item.ToString();

                result = $"{result}{separator}{value}";
                separator = "-";
            }

            return result;
        }

        public async Task<TEntity> GetById(TId id)
        {
            return await redisStorage.HashGet<TEntity>(key, id.ToString());
        }

        public async Task<IList<TEntity>> GetByIds(params TId[] ids)
        {
            var fields = ids.Select(m => m.ToString())
                .ToArray();

            return await redisStorage.HashGet<TEntity>(key, fields);
        }

        public async Task HashDelete(TId id)
        {
            await redisStorage.HashDelete(key, id.ToString());
        }

        public Task HashScan(Action<string, TEntity> iteratorAction, int pageSize = 10)
        {
            return redisStorage.HashScan(key, iteratorAction, pageSize);
        }

        public async Task<TEntity> GetById(TId id, string languageId)
        {
            var keyLanguage = string.IsNullOrEmpty(languageId) ? key : $"{key}_{languageId}";
            return await redisStorage.HashGet<TEntity>(keyLanguage, id.ToString());
        }

        public async Task<IList<TEntity>> GetByIds(TId[] ids, string languageId)
        {
            var keyLanguage = string.IsNullOrEmpty(languageId) ? key : $"{key}_{languageId}";
            var fields = ids.Select(m => m.ToString()).ToArray();
            return await redisStorage.HashGet<TEntity>(keyLanguage, fields);
        }

        public async Task HashDelete(TId id, string languageId)
        {
            var keyLanguage = string.IsNullOrEmpty(languageId) ? key : $"{key}_{languageId}";
            await redisStorage.HashDelete(keyLanguage, id.ToString());
        }

        public async Task KeyDelete(string languageId = "")
        {
            var keyLanguage = string.IsNullOrEmpty(languageId) ? key : $"{key}_{languageId}";
            var exist = await redisStorage.KeyExist(keyLanguage);
            if (exist)
            {
                await redisStorage.KeyDelete(keyLanguage);
            }
        }

        public async Task<TEntity> StringGet(string languageId = "")
        {
            return await redisStorage.StringGet<TEntity>(languageId);
        }

        public async Task<bool> StringSet(IList<TEntity> models, string languageId = "")
        {
            var keyLanguage = string.IsNullOrEmpty(languageId) ? key : $"{key}_{languageId}";
            await KeyDelete(keyLanguage);
            return await redisStorage.StringSet(keyLanguage, models);
        }
        public async Task<bool> StringSetCache(string models, string id)
        {
            return await redisStorage.StringSet(id, models);
        }


        public async Task<IList<TEntity>> GetAll()
        {
            return await redisStorage.HashGetAll<TEntity>(key);
        }

        public async Task<IList<TEntity>> GetAll(string languageId = "")
        {
            var keyLanguage = string.IsNullOrEmpty(languageId) ? key : $"{key}_{languageId}";
            return await redisStorage.HashGetAll<TEntity>(keyLanguage);
        }

        public async Task<bool> HashSet(string key, TEntity model, string languageId = "")
        {
            var keyLanguage = string.IsNullOrEmpty(languageId) ? key : $"{key}_{languageId}";
            return await redisStorage.HashSet(keyLanguage, key, model);
        }
    }
}
