﻿using InfraSystem.Cache.Interface;
using InfraSystem.Cache.Model;
using InfraSystem.Shared.RedisCache.Interfaces;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Redis.Implement
{
   public class ProductCache : BaseCache<ProductCacheModel, string>, IProductCache
    {
        // ichiba.products.code-052302722609
        // private const string KEY = "warehouse-location";
        public const string AllCacheKey = "ichiba.products.all-{0}";
        public const string CacheKeyByCommodityCode =  "ichiba.products.commodity-code-{0}";
        public const string CacheKeyByCode = "ichiba.products.code-{0}";
        public const string CacheKeyByPostOfficeAndCode = "ichiba.products.postoffice-{0}-code-{1}";
        public const string PrefixCacheKey = "ichiba.products";

        public ProductCache(IRedisStorage redisStorage)
            : base(redisStorage, PrefixCacheKey)
        {
        }

        public async Task ClearProductCache()
        {
            await redisStorage.StringSet(string.Format(AllCacheKey, false) , null);
            await redisStorage.StringSet(string.Format(AllCacheKey, true) , null);
            return;
        }

        public async Task<ProductCacheModel> GetByProductCodeAsync(string code)
        {
           return await redisStorage.StringGet<ProductCacheModel>(string.Format(CacheKeyByCode, code));
        }

        public async Task<bool> SetProductCache(ProductCacheModel model)
        {
           return await  redisStorage.StringSet(string.Format(CacheKeyByCode, model.Code) ,  model);
        }
    }
}
