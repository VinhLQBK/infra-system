﻿using InfraSystem.Cache.Interface;
using InfraSystem.Cache.Model;
using InfraSystem.Shared.RedisCache.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Redis.Implement
{
   public class WarehouseLocationCache : BaseCache<IList<WarehouseLocationCacheModel>, int>, IWarehouseLocationCache
    {
        private const string KEY = "warehouse-location";

        public WarehouseLocationCache(IRedisStorage redisStorage)
            : base(redisStorage, KEY)
        {
        }

        public async Task<bool> HashSet(string key, string model, string languageId = "")
        {
            return await redisStorage.HashSet(key, key, model);
        }

        public async Task<bool> StringSet(string key, IList<WarehouseLocationCacheModel> models, string languageId = "")
        {
            return await redisStorage.StringSet(key, models);
        }

        public async Task<bool> StringSetSinger(string key, WarehouseLocationCacheByLocationName model, string languageId = "")
        {
            return await redisStorage.StringSet(key, model);
        }

        public async Task<WarehouseLocationCacheByLocationName> StringGetSinger(string key)
        {
            return await redisStorage.StringGet<WarehouseLocationCacheByLocationName>(key);
        }

        public async Task<WarehouseLocationCacheModel> StringGetSingerByCustomer(string key)
        {
            return await redisStorage.StringGet<WarehouseLocationCacheModel>(key);
        }

        public async Task<bool> StringSetSingerByCustomer(string key, WarehouseLocationCacheModel model, string languageId = "")
        {
            return await redisStorage.StringSet(key, model);
        }

        public async Task<bool> StringSetByPostOfficeId(string key, IList<WarehouseLocationPostOffice> models, string languageId = "")
        {
            return await redisStorage.StringSet(key, models);
        }

        public async Task<IList<WarehouseLocationPostOffice>> StringGetByPostOffice(string key)
        {
            var data = await redisStorage.StringGet<IList<WarehouseLocationPostOffice>>(key);

            return data;
        }

        public async Task<bool> StringSetByUnknow(string key, IList<WarehouseLocationUnkown> models, string languageId = "")
        {
            return await redisStorage.StringSet(key, models);
        }

        public async Task<IList<WarehouseLocationUnkown>> StringGetByUnknow(string key)
        {
           var data = await redisStorage.StringGet<IList<WarehouseLocationUnkown>>(key);

            return data;
        }


        public async Task<bool> StringSetCal(string key, IList<CalculateWarehouseLocationCacheModel> models, string languageId = "")
        {
            return await redisStorage.StringSet(key, models);
        }

        public async Task<IList<CalculateWarehouseLocationCacheModel>> StringGetCal(string key)
        {
            return await redisStorage.StringGet<IList<CalculateWarehouseLocationCacheModel>>(key);
        }
    }
}
