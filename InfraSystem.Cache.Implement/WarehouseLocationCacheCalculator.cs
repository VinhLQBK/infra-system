﻿using InfraSystem.Cache.Interface;
using InfraSystem.Cache.Model;
using InfraSystem.Shared.RedisCache.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Redis.Implement
{
   public class WarehouseLocationCacheCalculator : BaseCache<IList<CalculateWarehouseLocationCacheModel>, int>, IWarehouseLocationCacheCalculator
    {
        private const string KEY = "warehouse-location-calculator";

        public WarehouseLocationCacheCalculator(IRedisStorage redisStorage)
            : base(redisStorage, KEY)
        {
        }

        public async Task<IList<CalculateWarehouseLocationCacheModel>> StringGetCal(string key)
        {
            return await redisStorage.StringGet<IList<CalculateWarehouseLocationCacheModel>>(key);
        }

        public async Task<bool> StringSetCal(string key, IList<CalculateWarehouseLocationCacheModel> models, string languageId = "")
        {
            return await redisStorage.StringSet(key, models);
        }
    }
}
