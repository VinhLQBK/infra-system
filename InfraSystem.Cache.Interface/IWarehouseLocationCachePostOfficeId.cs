﻿using InfraSystem.Cache.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Interface
{
    public interface IWarehouseLocationCachePostOfficeId : IBaseCache<IList<WarehouseLocationPostOffice>, int>
    {
        Task<bool> StringSetByPostOfficeId(string key, IList<WarehouseLocationPostOffice> models, string languageId = "");
    }
}
