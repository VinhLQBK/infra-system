﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Interface
{
    public interface IBaseCache<TEntity, TId>
    {
        Task<TEntity> GetById(TId id);
        Task<IList<TEntity>> GetByIds(params TId[] ids);
        Task HashDelete(TId id);
        Task HashScan(Action<string, TEntity> iteratorAction, int pageSize = 10);
        Task<TEntity> GetById(TId id, string languageId);
        Task<IList<TEntity>> GetByIds(TId[] ids, string languageId);
        Task HashDelete(TId id, string languageId);
        Task KeyDelete(string languageId = "");
        Task<IList<TEntity>> GetAll();
        Task<TEntity> StringGet(string languageId = "");
        Task<bool> StringSet(IList<TEntity> models, string languageId = "");
        Task<IList<TEntity>> GetAll(string languageId = "");
        Task<bool> HashSet(string key, TEntity model, string languageId = "");
        Task<bool> StringSetCache(string models, string languageId = "");
    }
}
