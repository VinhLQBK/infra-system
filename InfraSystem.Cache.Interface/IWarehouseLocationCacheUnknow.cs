﻿using InfraSystem.Cache.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Interface
{
    public interface IWarehouseLocationCacheUnknow : IBaseCache<IList<WarehouseLocationUnkown>, int>
    {
        Task<bool> StringSetByUnknow(string key, IList<WarehouseLocationUnkown> models, string languageId = "");
        Task<IList<WarehouseLocationUnkown>> StringGetByUnknow(string key);
    }
}
