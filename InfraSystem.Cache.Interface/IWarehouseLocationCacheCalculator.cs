﻿using InfraSystem.Cache.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Interface
{
    public interface IWarehouseLocationCacheCalculator : IBaseCache<IList<CalculateWarehouseLocationCacheModel>, int>
    {
        Task<IList<CalculateWarehouseLocationCacheModel>> StringGetCal(string key);
        Task<bool> StringSetCal(string key, IList<CalculateWarehouseLocationCacheModel> models, string languageId = "");
    }
}
