﻿using System.Linq;
using System.Threading.Tasks;
using Core.Specification.Abstract;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;

namespace InfraSystem.Wh.Service
{
    public class WarehouseService : IWarehouseService
    {
        private readonly IWarehouseRepository _warehouseRepository;

        public WarehouseService(IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
        }

        public async Task<Warehouse> GetByCode(string warehouseCode)
        {
            return _warehouseRepository.Find(new Specification<Warehouse>(x => x.Code.Equals(warehouseCode)))
                .FirstOrDefault();
        }
    }
}
