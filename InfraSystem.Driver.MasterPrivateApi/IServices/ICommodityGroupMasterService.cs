﻿using System.Threading.Tasks;

using InfraSystem.Domain.Master.Entity;
using InfraSystem.Driver.MasterPrivateApi.Common;

using RestEase;

namespace InfraSystem.Driver.MasterPrivateApi.IServices
{
    [Header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36")]
    [Header("Accept", "application/json,text/*")]
    public interface ICommodityGroupMasterService
    {
         [Get("/commodity-group/get-by-id")]
         Task<DataResult<CommodityGroup>> GetByIdAsync(string id);
        [Get("/commodity-group/get-by-code")]
         Task<DataResult<CommodityGroup>> GetByCodeAsync(string code);
    }
}
