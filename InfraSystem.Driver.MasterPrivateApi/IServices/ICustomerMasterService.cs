﻿using System.Collections.Generic;
using System.Threading.Tasks;

using InfraSystem.Domain.Master.Entity;
using InfraSystem.Driver.MasterPrivateApi.Common;

using RestEase;

namespace InfraSystem.Driver.MasterPrivateApi.IServices
{
    [Header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36")]
    [Header("Accept", "application/json,text/*")]
    public interface ICustomerMasterService
    {
        [Get("/customer/get-by-code")]
        Task<DataResult<Spcustomer>> GetByCodeAsync(string code);
        
        [Get("/customer/get-select-list")]
        Task<DataResult<IEnumerable<IChibaListItem>>> GetSelectListAsync();

        [Get("/customer/get-by-key")]
        Task<DataResult<Spcustomer>> GetByKeyAsync(string key);
    }
}
