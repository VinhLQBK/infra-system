﻿using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using System.Web;
using Microsoft.AspNetCore.Http;
using InfraSystem.Domain.Master.Entity;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{

    public interface IStateProvinceMasterService
    {
        Task<StateProvince> FindByCodeAsync(string countryId, string code);
        Task< StateProvince> GetByIdAsync(string id);
    }
    public class StateProvinceMasterService : BaseService, IStateProvinceMasterService
    {
        public StateProvinceMasterService(IMasterPrivHttpClient httpClient, IAuthorizeService authorizeservice, IWorkContext workContext)
            : base(httpClient, authorizeservice,workContext )
        {
        }

        public async Task<StateProvince> FindByCodeAsync(string countryId, string code)
        {
            string strQuery = "countryId="+ HttpUtility.UrlEncode(countryId)+"&code="+HttpUtility.UrlEncode(code);
            var response = await base.Get<DataResult<StateProvince>>(MasterPrivateApiEndPoints.StateProvince.FindByCode+"?"+strQuery);
            return response?.data;
        }
        
        public async Task<StateProvince> GetByIdAsync(string id)
        {
            var value = HttpUtility.UrlEncode(id);
            var response = await base.Get<DataResult<StateProvince >>(MasterPrivateApiEndPoints.StateProvince.GetById+"?id="+value);
            return response?.data ;
        }
    }
}
