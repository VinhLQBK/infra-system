﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.Linq;
using InfraSystem.Domain.Master.Entity;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{

    public interface ICountryMasterService
    {
        Task<IList<Country>> GetAllAsync();
        Task< Country> GetByIdAsync(string id);
        Task< Country> GetByCodeAsync(string code);
        Task< Country> FindByKeyAsync(string key);
    }
    public class CountryMasterService : BaseService, ICountryMasterService
    {
        public CountryMasterService(IMasterPrivHttpClient httpClient, IAuthorizeService authorizeservice, IWorkContext workContext) 
            : base(httpClient, authorizeservice, workContext)
        {
        }

        public async Task<IList<Country>> GetAllAsync()
        {
            var response = await base.Get<DataResult<IEnumerable<Country>>>(MasterPrivateApiEndPoints.Country.GetAll);
            return response?.data?.ToList();
        }
        
        public async Task<Country> GetByIdAsync(string id)
        {
            var value = HttpUtility.UrlEncode(id);
            var response = await base.Get<DataResult<Country >>(MasterPrivateApiEndPoints.Country.GetById+"?id="+value);
            return response?.data ;
        }
        public async Task<Country> GetByCodeAsync(string code)
        {
            var value = HttpUtility.UrlEncode(code);
            var response = await base.Get<DataResult<Country >>(MasterPrivateApiEndPoints.Country.GetByCode+"?code="+value);
            return response?.data;
        } 
        
        public async Task<Country> FindByKeyAsync(string key)
        {
            var value = HttpUtility.UrlEncode(key);
            var response = await base.Get<DataResult<Country >>(MasterPrivateApiEndPoints.Country.FindByKey+"?key="+value);
            return response?.data;
        }
    }
}
