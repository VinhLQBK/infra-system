﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using Microsoft.AspNetCore.Http;
using InfraSystem.Domain.Master.Entity;
using System.Web;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{
    public interface ICargoAddMasterService
    {
        Task<IList<CargoAddService>> GetByCodesAsync(List<string> codes);
        Task<IList<CargoAddService>> GetAllAsync();
        Task<bool> IsRequiredImageAsync(string cargoAddServiceId);
    }
    public class CargoAddMasterService : BaseService, ICargoAddMasterService
    {
        public CargoAddMasterService(
            IMasterPrivHttpClient httpClient,
            IAuthorizeService authorizeservice,
            IWorkContext workContext
        )
         : base(httpClient, authorizeservice,  workContext)
        {

        }

        public async Task<IList<CargoAddService>> GetByCodesAsync(List<string> codes)
        {
            var urlParam = base.BuildQueryParamater("codes", codes);
            var response = await base.Get<DataResult<List<CargoAddService>>>(MasterPrivateApiEndPoints.CargoAddService.GetByCodes + "?" + urlParam);
            return response?.data;
        }
        public async Task<IList<CargoAddService>> GetAllAsync()
        {
            var response = await base.Get<DataResult<List<CargoAddService>>>(MasterPrivateApiEndPoints.CargoAddService.GetAll);
            return response?.data;
        }

        public async Task<bool> IsRequiredImageAsync(string cargoAddServiceId)
        {
            var response = await base.Get<DataResult<bool>>(MasterPrivateApiEndPoints.CargoAddService.IsRequiredImage + "?id=" + HttpUtility.UrlEncode(cargoAddServiceId));
            return response?.data ?? false;
        }
    }
}
