﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using Microsoft.AspNetCore.Http;
using System.Web;
using InfraSystem.Domain.Master.Entity;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{
    public interface ICargoSpMasterService
    {
        Task<IList<IChibaListItem>> GetSelectListAsync();
        Task<CargoSpservice> GetByCodeAsync(string cargoSpServiceCode);

        Task<CargoSpservice> GetDefaultAsync();
        Task<IEnumerable<CargoSpservice>> GetAllAsync();
    }
    public class CargoSpMasterService : BaseService, ICargoSpMasterService
    {
        public CargoSpMasterService(IMasterPrivHttpClient httpClient, IAuthorizeService authorizeservice, IWorkContext workContext)
                : base(httpClient, authorizeservice, workContext)
        {
        }

        public async Task<IEnumerable<CargoSpservice>> GetAllAsync()
        {
            var response = await base.Get<DataResult<IEnumerable<CargoSpservice>>>(MasterPrivateApiEndPoints.CargoSpService.GetAll);
            return response?.data;
        }

        public async Task<CargoSpservice> GetByCodeAsync(string cargoSpServiceCode)
        {
            string urlQuery = $"code={HttpUtility.UrlEncode(cargoSpServiceCode)}";
            var response = await base.Get<DataResult<CargoSpservice>>(MasterPrivateApiEndPoints.CargoSpService.GetByCode + "?" + urlQuery);
            return response?.data;
        }

        public async Task<CargoSpservice> GetDefaultAsync()
        {
            var response = await base.Get<DataResult<CargoSpservice>>(MasterPrivateApiEndPoints.CargoSpService.GetDefault);
            return response?.data;
        }

        public async Task<IList<IChibaListItem>> GetSelectListAsync()
        {
            var response = await base.Get<DataResult<List<IChibaListItem>>>(MasterPrivateApiEndPoints.CargoSpService.GetSelectList);
            return response?.data;
        }
    }
}
