﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using Microsoft.AspNetCore.Http;
using System.Linq;
using InfraSystem.Domain.Master.Entity;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{
    public interface IIncotermMasterService
    {
        Task<IList<Incoterm>> GetAllAsync();
    }

    public class IncotermMasterService : BaseService, IIncotermMasterService
    {
        public IncotermMasterService(
            IMasterPrivHttpClient httpClient, 
            IAuthorizeService authorizeservice,
            IWorkContext workContext
        )
         : base(httpClient, authorizeservice, workContext )
        {
        }

        public async Task<IList<Incoterm>> GetAllAsync()
        {
            var response = await base.Get<DataResult<IEnumerable<Incoterm>>>(MasterPrivateApiEndPoints.Incoterm.GetAll);
            return response?.data?.ToList();
        }
    }
}
