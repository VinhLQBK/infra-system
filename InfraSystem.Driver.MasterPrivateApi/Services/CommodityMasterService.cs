﻿using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using System.Web;
using Microsoft.AspNetCore.Http;
using InfraSystem.Domain.Master.Entity;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{
    public interface ICommodityMasterService
    {
        Task<Commodity> GetCommodityByCodeAsync(string code);
        Task<Commodity> GetCommodityByNameAsync(string name);
    }
    public class CommodityMasterService : BaseService, ICommodityMasterService
    {
        public CommodityMasterService(IMasterPrivHttpClient httpClient, IAuthorizeService authorizeservice,IWorkContext workContext)
            : base(httpClient, authorizeservice, workContext)
        {
        }

        public async Task<Commodity> GetCommodityByCodeAsync(string code)
        {
            var query = "?code=" + HttpUtility.UrlEncode(code);
            var response = await base.Get<DataResult<Commodity>>(MasterPrivateApiEndPoints.Commodity.GetCommodityByCode + query);
            return response?.data;
        }
        public async Task<Commodity> GetCommodityByNameAsync(string name)
        {
            var query = "?name=" + HttpUtility.UrlEncode(name);
            var response = await base.Get<DataResult<Commodity>>(MasterPrivateApiEndPoints.Commodity.GetCommodityByName + query);
            return response?.data;
        }

    }
}
