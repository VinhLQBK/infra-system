﻿namespace InfraSystem.Driver.MasterPrivateApi.Common
{

    /// <summary>
    /// Auth info from header name
    /// </summary>
    public static class IChibaHeaderNames
    {
        public static readonly string LanguageId = "IChiba-Language-Id";
        public static readonly string PostOfficeId = "IChiba-PostOffice-Id";
        public static readonly string UserId = "IChiba-User-Id";
        public static readonly string UserName = "IChiba-User-Name";
    }

    /// <summary>
    /// Api Config
    /// </summary>
    public static class MasterPrivateApiEndPoints
    {
        public const string SettingKey = "MasterPrivateApiEndPoints";

        public class CargoAddService
        {
            public const string GetByCodes = "/cargo-add-service/get-by-codes";
            public const string GetAll = "/cargo-add-service/get-all";
            public const string IsRequiredImage = "/cargo-add-service/is-required-image";
        }
        public class Commodity
        {
            public const string GetCommodityByCode = "/commodity/get-by-code";
            public const string GetCommodityByName = "/commodity/get-by-name";
        }

        public class Incoterm
        {
            public const string GetAll = "/incoterm/get-all";
        }

        public class PostOffice
        {
            public const string GetAll = "/post-office/get-all";
            public const string GetByCountry = "/post-office/get-by-country";
            public const string GetByCode = "/post-office/get-by-code";
            public const string GetCountryCodeByPostCode = "/post-office/get-country-code-by-post-code";
        }

        public class EventType
        {
            public const string GetAll = "/event-type/get-all";
            public const string GetAllGroup = "/event-type/get-all-group";

        }
        public class CargoSpService
        {
            public const string GetSelectList = "/cargo-sp-service/get-select-list";
            public const string GetByCode = "/cargo-sp-service/get-by-code";
            public const string GetDefault = "/cargo-sp-service/get-default";
            public const string GetAll= "/cargo-sp-service/get-all";
        }
        public class SpPartner
        {
            public const string GetSelectShippingMethods = "/sp-partner/get-select-shipping-method";
            public const string GetSelectPartnerByTypes = "/sp-partner/get-select-partner-types";
            public const string GetSelectPartners = "/sp-partner/get-select-partners";
        }

        public class Product
        {
            public const string GetSelectList = "/product/get-select-list";
        }
        //public class SpCustomer
        //{
        //    public const string GetAll = "/customer/get-all";

        //}
        //public class Shipper
        //{
        //    public const string GetByCode = "/shipper/get-by-code";
        //}
        public class Country
        {
            public const string GetAll = "/country/get-all";
            public const string GetById = "/country/get-by-id";
            public const string GetByCode = "/country/get-by-code";
            public const string FindByKey = "/country/find-by-key";
        }

        public class StateProvince
        {
            public const string GetById = "/state-province/get-by-id";
            public const string FindByCode = "/state-province/find-by-code";
        }
        public class Ward
        {
            public const string GetById = "/ward/get-by-id";
            public const string FindByCode = "/ward/find-by-code";
        }
        public class District
        {
            public const string GetById = "/district/get-by-id";
            public const string FindByCode = "/district/find-by-code";
        }
        //public class Consignee { 
        //    public const string GetConsigneeByCode = "/consignee/get-by-code";
        //    }
    }


    /// <summary>
    /// Represents startup hosting configuration parameters
    /// </summary>
    public partial class HostingConfig
    {
        public const string Hosting = "Hosting";

        /// <summary>
        /// Gets or sets custom forwarded HTTP header (e.g. CF-Connecting-IP, X-FORWARDED-PROTO, etc)
        /// </summary>
        public string ForwardedHttpHeader { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use HTTP_CLUSTER_HTTPS
        /// </summary>
        public bool UseHttpClusterHttps { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use HTTP_X_FORWARDED_PROTO
        /// </summary>
        public bool UseHttpXForwardedProto { get; set; }


        public ApisConfig ApisConfig { get; set; }
    }

    public partial class ApisConfig
    {
        public string AutoNumber { get; set; }

        // Master 
        public string MasterApi { get; set; }
        public string MasterApiPrivate { get; set; }

        // OP 
        public string OpApi { get; set; }
        public string OpApiPrivate { get; set; }
        public string PartnerApi { get; set; }
        public string PartnerApiPrivate { get; set; }

        public string WhApi { get; set; }
        public string WhApiPrivate { get; set; }

        // MQ
        /// <summary>
        /// Api nhận thông tin => Mesage Queue
        /// </summary>
        public string MqApi { get; set; }
        /// <summary>
        /// Đọc thông tin từ redis do worker đã xử lý
        /// </summary>
        public string MqApiPrivate { get; set; }
        /// <summary>
        /// Đọc thông tin từ redis do worker đã xử lý
        /// </summary>
        public string MqApiPublic { get; set; }




    }
}
