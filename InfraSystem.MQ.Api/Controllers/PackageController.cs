﻿using System;
using System.Net;
using System.Threading.Tasks;
using InfraSystem.MQ.Contracts;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace InfraSystem.MQ.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackageController : ControllerBase
    {
        private readonly IBus _bus;
        private readonly ILogger<PackageController> _logger;

        public PackageController(IBus bus, ILogger<PackageController> logger)
        {
            _bus = bus;
            _logger = logger;
        }

        [Route("add-package")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> AddPackage([FromBody] AddPackageMessage request)
        {
            try
            {
                await _bus.Publish(request);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok();
        }
    }
}
