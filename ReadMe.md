InfraSystem for EFEX
===========================

Infrastructure system:

- MQ Api    :  Webhook to recive push message then push to RabbitMQ
- MQ Worker :  Process Message from webhook
- MQ Private:  Get Result that processed from Worker

## CI/CD Key word

    - [build_mq] [deploy_mq]              Build/Build+Deploy MQ Api
    - [build_mq_priv] [deploy_mq_priv]    Build/Build+Deploy MQ Private Data Api
    - [build_worker] [deploy_worker]      Build/Build+Deploy MQ Private Data Api
    - [build_all] [deploy_all]            Build/Build+Deploy All Api