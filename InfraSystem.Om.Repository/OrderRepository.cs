﻿using Core.Repository.Abstract;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IRepository;

namespace InfraSystem.Om.Repository
{
    public class OrderRepository: BaseRepository<IchibaCustomerContext, Order>, IOrderRepository
    {
        public OrderRepository(IchibaCustomerContext dbContext) : base(dbContext)
        {
        }
    }
}
