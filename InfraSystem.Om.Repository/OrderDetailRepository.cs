﻿using Core.Repository.Abstract;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IRepository;

namespace InfraSystem.Om.Repository
{
    public class OrderDetailRepository : BaseRepository<IchibaCustomerContext, Orderdetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(IchibaCustomerContext dbContext) : base(dbContext)
        {
        }
    }
}
