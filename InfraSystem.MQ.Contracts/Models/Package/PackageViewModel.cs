﻿using System.Collections.Generic;

namespace InfraSystem.MQ.Contracts.Models
{
    public class PackageViewModel
    {
        public string TrackingCode { get; set; }
        public string ShipmentCode { get; set; }
        public string Name { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public int? NoOfPiece { get; set; }
        public decimal? CodFee { get; set; }
        public List<PackageViewModel> PackageViewModels { get; set; }
    }
}
