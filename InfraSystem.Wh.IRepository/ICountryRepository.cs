﻿using Core.Repository.Interface;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.IRepository
{
    public interface ICountryRepository : IRepository<Country>
    {

    }
}
