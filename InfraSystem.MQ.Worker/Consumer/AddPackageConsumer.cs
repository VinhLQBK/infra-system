﻿using InfraSystem.MQ.Contracts;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InfraSystem.Om.IService;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IService;
using Microsoft.Extensions.Logging;
using Core.Repository.Interface;
using Microsoft.VisualBasic;
using Core.Common;
using InfraSystem.Common;
using InfraSystem.MQ.Worker.AutoMappers;
using MassTransit.Initializers;

namespace InfraSystem.MQ.Worker.Consumer
{
    public class AddPackageConsumer : IConsumer<AddPackageMessage>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<AddPackageConsumer> _logger;
        private readonly IPackageService _packageService;
        private readonly IPackageDetailService _packageDetailService;
        private readonly IOrderService _orderService;
        private readonly IOrderDetailService _orderDetailService;
        private readonly IWarehouseService _warehouseService;
        private readonly IServiceChargeService _serviceChargeService;
        private readonly IOrderServiceMappingService _orderServiceMappingService;
        private readonly ICategoryService _categoryService;
        private readonly ICountryService _countryService;
        private readonly IServiceChargePackageMappingService _serviceChargePackageMappingService;

        public AddPackageConsumer(
            ILogger<AddPackageConsumer> logger,
            IUnitOfWork unitOfWork,
            IPackageService packageService,
            IPackageDetailService packageDetailService,
            IOrderService orderService,
            IOrderDetailService orderDetailService,
            IWarehouseService warehouseService,
            IServiceChargeService serviceChargeService,
            IOrderServiceMappingService orderServiceMappingService,
            ICategoryService categoryService,
            ICountryService countryService,
            IServiceChargePackageMappingService serviceChargePackageMappingService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _orderService = orderService;
            _orderDetailService = orderDetailService;
            _packageService = packageService;
            _packageDetailService = packageDetailService;
            _warehouseService = warehouseService;
            _serviceChargeService = serviceChargeService;
            _orderServiceMappingService = orderServiceMappingService;
            _categoryService = categoryService;
            _countryService = countryService;
            _serviceChargePackageMappingService = serviceChargePackageMappingService;
        }

        public async Task Consume(ConsumeContext<AddPackageMessage> context)
        {
            var requestData = context.Message;
            try
            {
                #region Get Order

                var order = await _orderService.GetByShipmentCode(requestData.PackageViewModel.ShipmentCode);

                #endregion

                if (order == null)
                {
                    await AddPackageNotMappingOrder(requestData);
                }
                else
                {
                    await AddPackageMappingOrder(requestData);
                }

                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }

        private async Task AddPackageMappingOrder(AddPackageMessage requestData)
        {
            try
            {
                var createBy = "SYSTEM";
                var warehouse = await _warehouseService.GetByCode("");

                #region Order

                var order = await _orderService.GetByShipmentCode(requestData.PackageViewModel.ShipmentCode);
                var orderDetails = await _orderDetailService.GetByOrderId(order.Id);
                var orderServiceMappings = await _orderServiceMappingService.GetByOrderId(order.Id);
                var serviceCharges =
                    await _serviceChargeService.GetById(orderServiceMappings.Select(x => x.OrderServiceId).ToList());

                #endregion

                #region Add Package

                #region Generate code
                const int flag = 1;
                var counter = 1;
                int? binLocationId = null;
                var packageLeast = await _packageService.GetLeast(warehouse.Code);

                if (packageLeast != null)
                {
                    binLocationId = packageLeast.BinLocationId;
                    if (packageLeast.Code != null)
                    {
                        var arrCode = packageLeast.Code.Substring(packageLeast.Code.Length - 4, 4);
                        if (arrCode.Length > 1) counter = arrCode.AsInt() + flag;
                    }
                    else
                    {
                        counter = 100;
                    }
                }

                #endregion

                var package = new Package
                {
                    TrackingCode = requestData.PackageViewModel.TrackingCode,
                    Code = string.Join(
                        string.Empty,
                        warehouse.Code,
                        DateTime.UtcNow.ToString("yyMMdd"),
                        counter.ToString().PadLeft(4, '0')),
                    WarehouseId = warehouse.Id,
                    WarehouseCode = warehouse.Code,
                    BinLocationId = binLocationId,
                    Status = EnumDefine.PackageStatus.Untapped.GetHashCode(),
                    CreatedBy = createBy,
                    CreatedDate = DateTime.UtcNow,
                    ProcessedDate = DateTime.UtcNow
                };

                #endregion

                #region Trường hợp có dữ liệu trên OM

                var region = ConstantDefine.Province_Bac;

                if (order.OrderType.Equals(ConstantDefine.ORDER_TYPE_TRANSPORT))
                {
                    //#region Đơn vận chuyển, mỗi order chỉ có một mã Tracking duy nhất

                    //#region Tạo package parent và mapping package detail

                    //var firstOrderPackage = order.OrderPackages.FirstOrDefault();
                    //if (firstOrderPackage != null)
                    //{
                    //    package.Length = firstOrderPackage.Length;
                    //    package.Width = firstOrderPackage.Width;
                    //    package.Height = firstOrderPackage.Height;
                    //    package.Weight = firstOrderPackage.Weight / 1000m;
                    //}

                    //_packageService.Add(package);
                    //var packageDetail = order.ToPackageDetailModelTransport(package);
                    //packageDetail.Length = package.Length ?? 0;
                    //packageDetail.Width = package.Width ?? 0;
                    //packageDetail.Height = package.Height ?? 0;
                    //packageDetail.Weight = package.Weight ?? 0;

                    //_packageDetailService.Add(packageDetail);
                    //_unitOfWork.Commit();

                    //if (firstOrderPackage != null)
                    //{
                    //    firstOrderPackage.Code = package.Id.ToString();
                    //    package.RefPackageId = firstOrderPackage.Id;
                    //}

                    //#endregion

                    //if (order.OrderPackages.Count > 1)
                    //{
                    //    #region Tạo package child và mapping products

                    //    var indexPackage = 1;
                    //    foreach (var orderPackage in order.OrderPackages)
                    //    {
                    //        var packageChild = new Package
                    //        {
                    //            TrackingCode = $"{request.TrackingCode}-{indexPackage}",
                    //            Code = string.Join(
                    //                string.Empty,
                    //                warehouse.Code,
                    //                DateTime.UtcNow.AddMilliseconds(indexPackage).ToString("yyMMdd"),
                    //                counter.ToString().PadLeft(4, '0')),
                    //            WarehouseId = warehouse.Id,
                    //            WarehouseCode = warehouse.Code,
                    //            BinLocationId = binLocationId,
                    //            Status = EnumDefine.PackageStatus.Inventory.GetHashCode(),
                    //            CreatedBy = _currentContext.UserId,
                    //            CreatedDate = DateTime.UtcNow.AddMilliseconds(indexPackage),
                    //            Length = orderPackage.Length,
                    //            Width = orderPackage.Width,
                    //            Height = orderPackage.Height,
                    //            Weight = orderPackage.Weight / 1000m,
                    //            ServiceChargeCode = order.ServiceChargeCode
                    //        };

                    //        #region Thêm sản phẩm cho gói con
                    //        var productsPackage = orderPackage.OrderPackageProducts.ToList();
                    //        if (productsPackage != null && productsPackage.Any())
                    //        {
                    //            foreach (var product in productsPackage)
                    //            {
                    //                if (product.DataType != null) continue;

                    //                var categoryId = 0;
                    //                var category = _categoryService.GetByNames(new List<string> { product.NameCustom });
                    //                if (category.Any())
                    //                {
                    //                    categoryId = category.First().Id;
                    //                }

                    //                var productModel = new Product
                    //                {
                    //                    SourceId = product.Id,
                    //                    Name = product.Name,
                    //                    NameCustom = product.NameCustom,
                    //                    CategoryId = categoryId > 0 ? categoryId : (int?)null,
                    //                    Weight = product.Weight / 1000m,
                    //                    Images = product.Image,
                    //                    Price = product.Price,
                    //                    Tax = product.Tax,
                    //                    QtyPerUnit = product.Qty,
                    //                    ProductUnit = product.Qty.ToString(),
                    //                    CreatedBy = _currentContext.UserId,
                    //                    CreatedDate = DateTime.UtcNow,
                    //                    PriceStandard = product.PriceStandard ?? 0,
                    //                    Package = packageChild,
                    //                    PackageDetail = packageDetail
                    //                };

                    //                _productService.Add(productModel);
                    //            }
                    //        }

                    //        #endregion

                    //        package.InverseParent.Add(packageChild);
                    //        _packageService.Add(packageChild);

                    //        orderPackage.Code = packageChild.Id.ToString();

                    //        indexPackage++;
                    //    }

                    //    #endregion
                    //}
                    //else
                    //{
                    //    #region Thêm sản phẩm cho gói cha
                    //    var orderPackageProducts = order.OrderPackages.Select(x => x.OrderPackageProducts).SelectMany(pgItem => pgItem).ToList();
                    //    if (orderPackageProducts != null && orderPackageProducts.Any())
                    //    {
                    //        foreach (var product in orderPackageProducts)
                    //        {
                    //            if (product.DataType != null) continue;

                    //            var categoryId = 0;
                    //            var category = _categoryService.GetByNames(new List<string> { product.NameCustom });
                    //            if (category.Any())
                    //            {
                    //                categoryId = category.First().Id;
                    //            }

                    //            var productModel = new Product
                    //            {
                    //                SourceId = product.Id,
                    //                Name = product.Name,
                    //                NameCustom = product.NameCustom,
                    //                CategoryId = categoryId > 0 ? categoryId : (int?)null,
                    //                Weight = product.Weight / 1000m,
                    //                Images = product.Image,
                    //                Price = product.Price,
                    //                Tax = product.Tax,
                    //                QtyPerUnit = product.Qty,
                    //                ProductUnit = product.Qty.ToString(),
                    //                CreatedBy = _currentContext.UserId,
                    //                CreatedDate = DateTime.UtcNow,
                    //                PriceStandard = product.PriceStandard ?? 0,
                    //                Package = package,
                    //                PackageDetail = packageDetail
                    //            };

                    //            _productService.Add(productModel);
                    //        }
                    //    }

                    //    #endregion
                    //}

                    //await GetIngredient(packageDetail.Product.ToList());

                    //#endregion
                }
                else
                {
                    package.ServiceChargeCode = string.Join(',',serviceCharges.Select(x => x.Code).ToArray());
                    package.Region = region;
                   
                    if (package.Id <= 0)
                    {
                        await _packageService.AddPackage(package);
                        _unitOfWork.Commit();
                    }

                    var packageDetail = PackageAdapter.ToPackageDetailFromOrderEcm(order, orderDetails, package, createBy);

                    foreach (var serviceCharge in serviceCharges)
                    {
                        var serviceChargeMapping = new ServiceChargePackageMapping
                        {
                            ServiceChargeId = serviceCharge.Id,
                            PackageDetail = packageDetail,
                            Status = EnumDefine.StatusServiceChargeMapping.Finished.GetHashCode()
                        };

                        await _serviceChargePackageMappingService.AddServiceChargePackageMapping(serviceChargeMapping);
                    }

                    var productTypes = packageDetail.Product.Select(x => x.NameCustom);
                    var categories = await _categoryService.GetByNames(productTypes.ToList());

                    var productOrigins = packageDetail.Product.Select(x => x.OriginCustom);
                    var origins = await _countryService.GetByCode(productOrigins.ToList());

                    foreach (var product in packageDetail.Product)
                    {
                        var category = categories.FirstOrDefault(x => x.Name.ToLower().Equals(product.NameCustom.ToLower()));
                        if (category != null)
                        {
                            product.CategoryId = category.Id;
                            // Cập nhật tiền phí VC và phụ thu cho đơn ECM
                            //await UpdateShippingUnitGlobalAndPriceStandardScan(packageDetail, category, product);
                        }

                        var origin = origins.FirstOrDefault(x => x.Code.ToLower().Equals(product.OriginCustom.ToLower()));
                        if (origin != null) product.Origin = origin.Id;
                    }

                    await _packageDetailService.AddPackageDetail(packageDetail);
                    _unitOfWork.Commit();
                }

                await _packageService.AddPackage(package);

                #endregion

                _unitOfWork.Commit();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception.Message);
            }
        }

        private async Task AddPackageNotMappingOrder(AddPackageMessage requestData)
        {
            throw new NotImplementedException();
        }
    }
}
