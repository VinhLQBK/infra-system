﻿namespace InfraSystem.MQ.Worker.Configs
{
    public class QueueSettings
    {
        public string HostName { get; set; }
        public string VirtualHost { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Environment { get; set; }
        public string WarehousePackageNameQueue { get; set; }
    }
}
