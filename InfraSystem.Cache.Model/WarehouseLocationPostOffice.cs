﻿using ProtoBuf;

namespace InfraSystem.Cache.Model
{
    [ProtoContract]
    public class WarehouseLocationPostOffice
    {
        [ProtoMember(1)]
        public string Id { get; set; }
        [ProtoMember(2)]
        public string Code { get; set; }
        [ProtoMember(3)]
        public string Name { get; set; }
        [ProtoMember(4)]
        public decimal? CapacityMaxWeight { get; set; }
        [ProtoMember(5)]
        public decimal? CapacityMinWeight { get; set; }
        [ProtoMember(6)]
        public decimal? CapacityLocation { get; set; }
        [ProtoMember(7)]
        public decimal? remainingWeight { get; set; }
        [ProtoMember(10)]
        public decimal? Width { get; set; }
        [ProtoMember(11)]
        public decimal? Height { get; set; }
        [ProtoMember(12)]
        public decimal? Length { get; set; }
        [ProtoMember(13)]
        public string PostOfficeId { get; set; }
        [ProtoMember(14)]
        public int? X { get; set; }
        [ProtoMember(15)]
        public int? Y { get; set; }
        [ProtoMember(16)]
        public string AreaId { get; set; }
        [ProtoMember(17)]
        public string CargoType { get; set; }
        [ProtoMember(18)]
        public string CargoSpservice { get; set; }
        [ProtoMember(19)]
        public string CargoShippingMethod { get; set; }
        [ProtoMember(20)]
        public string CustomerId { get; set; }
        [ProtoMember(21)]
        public string ConsigneeCountry { get; set; }
        [ProtoMember(22)]
        public string ConsigneeProvince { get; set; }
        [ProtoMember(23)]
        public bool? Unknown { get; set; }
        [ProtoMember(24)]
        public bool? Status { get; set; }
    }
}
