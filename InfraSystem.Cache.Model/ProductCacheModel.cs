﻿using ProtoBuf;

namespace InfraSystem.Cache.Model
{
    [ProtoContract]
    public class ProductCacheModel
    { 
        [ProtoMember(0)]
        public string Id { get; set; } // nvarchar(50)
        [ProtoMember(1)]
        public string PostOfficeId { get; set; } // nvarchar(50)
        [ProtoMember(2)]
        public string Code { get; set; } // nvarchar(50)
        [ProtoMember(3)]
        public string Name { get; set; } // nvarchar(500)
        [ProtoMember(4)]
        public string Description { get; set; } // nvarchar(500)
    }
}
