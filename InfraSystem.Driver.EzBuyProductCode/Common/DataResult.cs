﻿using System.Collections.Generic;
using System.Net;

namespace InfraSystem.Driver.EzBuyProductCode.Common
{
   public class DataResult
    {
        public bool success { get; set; }

        public string code { get; set; }

        public int httpStatusCode { get; set; }

        public string title { get; set; }

        public string message { get; set; }

        public object data { get; set; }

        public Dictionary<string, IEnumerable<string>> errors { get; set; }

        public DataResult()
        {
            success = true;
            httpStatusCode = (int)HttpStatusCode.OK;
            errors = new Dictionary<string, IEnumerable<string>>();
        }
    }
    public class DataResult<T> : DataResult
    {
        public new T data { get; set; }
    }
}
