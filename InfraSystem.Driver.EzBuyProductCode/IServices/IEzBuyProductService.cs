﻿using InfraSystem.Driver.EzBuyProductCode.Common;

using RestEase;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Driver.EzBuyProductCode.IServices
{
    [Header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36")]
    [Header("Accept", "application/json,text/*")]
    public interface IEzBuyProductService
    {
        [Get("/api/Product/Data/{code}")]
        Task<DataResult<EzProduct>> GetByCodeAsync([Path] string code);
    }
    public class EzProduct
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Seller { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public List<string> Images { get; set; }
    }
}
