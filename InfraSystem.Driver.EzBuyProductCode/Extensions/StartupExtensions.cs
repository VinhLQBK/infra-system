﻿using InfraSystem.Driver.EzBuyProductCode.IServices;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Polly;

using RestEase.HttpClientFactory;

using System;

namespace InfraSystem.Driver.EzBuyProductCode.Extensions
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddEzProductApi(this IServiceCollection services, IConfiguration configuration)
        {
            //Config
            var hostingConfig = configuration.GetValue<string>("Hosting:ApisConfig:EzProduct");

            services.AddEzService<IEzBuyProductService>(hostingConfig);

            return services;
        }


        private static IServiceCollection AddEzService<T>(this IServiceCollection services, string baseUrl) where T : class
        {

            services.AddRestEaseClient<T>(baseUrl)
                // .AddHttpMessageHandler<MasterPrivateWebWorkContextDelegatingHandler>()
                .AddTransientHttpErrorPolicy(builder =>
                    builder.WaitAndRetryAsync(new[]
                    {
                        TimeSpan.FromMilliseconds(100),
                        TimeSpan.FromMilliseconds(500),
                        TimeSpan.FromSeconds(2)
                    }));
            return services;
        }

    }
}
