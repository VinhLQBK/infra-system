﻿ 
using IdentityModel;

using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using InfraSystem.Driver.Translate.Common;
using IChiba.Caching;
using IChiba.Core;
using IChiba.Core.Infrastructure;

namespace InfraSystem.Driver.Translate.Common
{
    
    public class MasterPrivateWebWorkContextDelegatingHandler : DelegatingHandler
    {
        private   IWorkContext _workContext;
          
        public MasterPrivateWebWorkContextDelegatingHandler(IWorkContext workContext)
        {
            _workContext = workContext;
        }


        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // if(_workContext == null ) _workContext = EngineContext.Current.Resolve<IWorkContext>();

            string langId = _workContext?.LanguageId;
            string postOfficeId = "";
            string userId = _workContext?.UserId;
            string userName = _workContext?.UserName;

            request.Headers.Add(IChibaHeaderNames.LanguageId, langId);
            request.Headers.Add(IChibaHeaderNames.PostOfficeId, postOfficeId);
            request.Headers.Add(IChibaHeaderNames.UserId, userId);
            request.Headers.Add(IChibaHeaderNames.UserName, userName);

            return await base.SendAsync(request, cancellationToken);
                      // .ConfigureAwait(false);
        } 
    }
}
