﻿namespace InfraSystem.Driver.Translate.Common
{

    /// <summary>
    /// Auth info from header name
    /// </summary>
    public static class IChibaHeaderNames
    {
        public static readonly string LanguageId = "IChiba-Language-Id";
        public static readonly string PostOfficeId = "IChiba-PostOffice-Id";
        public static readonly string UserId = "IChiba-User-Id";
        public static readonly string UserName = "IChiba-User-Name";
    }

    public class AppSettings
    {
        public AppSettings()
        {
        }

        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }
        public string Target { get; set; }

        public string TranslateEndpoint { get; set; }

        public static AppSettings DefaultSetting = new AppSettings() {
             BaseUrl = "https://api-translate.systran.net",
             ApiKey = "a3c93bf3-cf5a-4a7d-a6d0-897f328d6c51", 
             TranslateEndpoint = "/translation/text/translate",
             Target = "en"
            };
    }




}
