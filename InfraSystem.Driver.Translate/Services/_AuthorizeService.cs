﻿using System.Threading.Tasks;

namespace InfraSystem.Driver.Translate.Services
{
    public interface IAuthorizeService
    {
        Task<string> GetAuthorizeToken();
    }

    public class AuthorizeService : IAuthorizeService
    {
        public async Task<string> GetAuthorizeToken()
        {
            return await Task.FromResult("");
        }
    }
}
