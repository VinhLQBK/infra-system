﻿using System.Threading.Tasks;
using InfraSystem.Driver.Translate.Common;
using Microsoft.Extensions.Options;
using System.Web;
using System.Collections.Generic;
using System.Linq;

namespace InfraSystem.Driver.Translate.Services
{
    public interface ITranslateService
    {
        Task<string[]> TranslateAsync(params string[] codes);
    }
    public class TranslateService : BaseService, ITranslateService
    {
        private AppSettings _appSetting;

        public TranslateService(
            ITranslateDriverHttpClient httpClient,
            IAuthorizeService authorizeservice,
            IOptions<AppSettings> options
        )
        : base(httpClient, authorizeservice)
        {
            this._appSetting = options?.Value;
        }

        public async Task<string[]> TranslateAsync(params string[] codes)
        {
            if (codes == null || codes.Length == 0)
            {
                return null;
            }
            string url = string.Empty;
            // url += _appSetting?.BaseUrl?.NullEmpty() ?? AppSettings.DefaultSetting.BaseUrl;
            url += _appSetting?.TranslateEndpoint?.NullEmpty() ?? AppSettings.DefaultSetting.TranslateEndpoint;
            url += $"?key={HttpUtility.UrlEncode(_appSetting?.ApiKey?.NullEmpty() ?? AppSettings.DefaultSetting.ApiKey)}&target={HttpUtility.UrlEncode(_appSetting.Target.NullEmpty() ?? AppSettings.DefaultSetting.Target)}";
            foreach (var code in codes)
            {
                url += $"&input={HttpUtility.UrlEncode(code)}";
            }
            var response = await base.Get<TranslateResult>(url);
            return response?.outputs?.Select(c => c.output)?.ToArray();
        }
    }



    public class Output
    {
        public string output { get; set; }
    }

    public class TranslateResult
    {
        public List<Output> outputs { get; set; }
    }

}
