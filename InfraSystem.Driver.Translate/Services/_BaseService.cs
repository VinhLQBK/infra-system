﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

using InfraSystem.Driver.Translate.Common;
using Newtonsoft.Json;

namespace InfraSystem.Driver.Translate.Services
{
    public class BaseService
    {
        private readonly ITranslateDriverHttpClient _httpClient;
        private readonly IAuthorizeService _authorizeservice;

        public BaseService(
            ITranslateDriverHttpClient httpClient,
            IAuthorizeService authorizeservice
        )
        {
            _httpClient = httpClient;
            _authorizeservice = authorizeservice; 
        }
 
        protected async Task<string> AuthorizationToken()
        {
            if (_authorizeservice == null)
            {
                return null;
            }
            return await _authorizeservice.GetAuthorizeToken();
        }
        
        protected virtual async Task<HttpResponseMessage> PostAsync<T>(
            string uri,
            T request,
            string requestId = null,
            string authorizationMethod = "Bearer",
            Dictionary<string, string> headers = null)
        {
            var authorizationToken = string.Empty;
            try
            {
                authorizationToken = await AuthorizationToken();
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (_httpClient == null)
            {
                throw new NullReferenceException(nameof(_httpClient));
            }

            return await _httpClient.PostAsync(
                uri: uri,
                item: request,
                authorizationToken: authorizationToken,
                requestId: requestId,
                authorizationMethod: authorizationMethod,
                headers: headers);
        }

        protected virtual async Task<TResponse> PostAsync<TResponse, TRequest>(
            string uri,
            TRequest request,
            string requestId = null,
            string authorizationMethod = "Bearer",
            Dictionary<string, string> headers = null)
        {

            var response = await PostAsync(
                uri: uri,
                request: request,
                requestId: requestId,
                authorizationMethod: authorizationMethod,
                headers: headers);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<TResponse>(content);

            return data;
        }

        protected virtual async Task<TResponse> Get<TResponse>(
            string uri,
            string authorizationToken = null,
            Dictionary<string, string> headers = null)
        {
           
            var response = await _httpClient.GetStringAsync(
               uri: uri,
               authorizationToken: authorizationToken,
               headers: headers);
            var data = JsonConvert.DeserializeObject<TResponse>(response);

            return data;
        }

        protected virtual async Task<TResponse> Get<TResponse>(
            string uri,
            Func<string, string> executeBeforeParse,
            string authorizationToken = null,
             Dictionary<string, string> headers = null)
        {
           

            var response = await _httpClient.GetStringAsync(
                uri: uri,
                authorizationToken: authorizationToken,
                headers: headers);

            if (executeBeforeParse != null)
            {
                response = executeBeforeParse.Invoke(response);
            }

            var data = JsonConvert.DeserializeObject<TResponse>(response);

            return data;
        }

        protected string Join(char separator, params object[] inputs)
        {
            if (inputs == null)
            {
                return null;
            }

            var inputStandard = inputs.Where(m => m != null)
                .Select(m => m.ToString().Trim(separator));

            return string.Join(separator, inputStandard);
        }

        /// <summary>
        /// Trả về chuỗi url paramater :  key=value&key=value
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected string BuildParameter(IDictionary<string, object> parameters)
        {
            var separator = "&";
            var result = string.Empty;

            foreach (var item in parameters)
            {
                if (item.Value == null || string.IsNullOrWhiteSpace(item.Value.ToString()))
                {
                    continue;
                }
                var value = HttpUtility.UrlEncode(item.Value.ToString());

                result = $"{result}{separator}{item.Key}={value}";
            } 
            return result.StartsWith("&")  ? result.Substring(1 , result.Length -1 ) : result;
        }

        /// <summary>
        /// Trả về chuỗi url paramater :  paramName=value1&paramName=value2
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="datas"></param>
        /// <returns></returns>
        protected string BuildQueryParamater(string paramName, List<string> datas)
        { 
              var separator = "&";
            var result = string.Empty;

            foreach (var item in datas)
            {
                if (item  == null || string.IsNullOrWhiteSpace(item.ToString()))
                {
                    continue;
                }
                var value = HttpUtility.UrlEncode(item.Trim());

                result = $"{result}{separator}{paramName}={value}";
            } 
            return result.StartsWith("&")  ? result.Substring(1 , result.Length -1 ) : result;
        }

    }
}
