﻿using Core.Repository.Interface;
using InfraSystem.Om.Domain;

namespace InfraSystem.Om.IRepository
{
    public interface IOrderServiceMappingRepository : IRepository<OrderServiceMapping>
    {
    }
}
