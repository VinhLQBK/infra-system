﻿using System.Collections.Generic;
using System.Linq;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IService;
using System.Threading.Tasks;
using Core.Specification.Abstract;
using InfraSystem.Om.IRepository;

namespace InfraSystem.Om.Service
{
    public class OrderServiceMappingService : IOrderServiceMappingService
    {
        private readonly IOrderServiceMappingRepository _orderServiceMappingRepository;

        public OrderServiceMappingService(IOrderServiceMappingRepository orderServiceMappingRepository)
        {
            _orderServiceMappingRepository = orderServiceMappingRepository;
        }

        public async Task<List<OrderServiceMapping>> GetByOrderId(int orderId)
        {
            return _orderServiceMappingRepository
                .Find(new Specification<OrderServiceMapping>(x => x.OrderId.Equals(orderId))).ToList();
        }
    }
}
