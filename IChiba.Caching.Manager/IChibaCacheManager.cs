﻿using Autofac;
using EasyCaching.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Caching.Manager
{
    public partial class IChibaCacheManager : IIChibaCacheManager
    {
        public IHybridCachingProvider HybridProvider { get; }

        public IChibaCacheManager(
            IHybridCachingProvider hybridProvider)
        {
            HybridProvider = hybridProvider;
        }

        public virtual T GetDb<T>(string key, Func<T> acquirer, int? cacheTime = null)
        {
            var duration = cacheTime.HasValue && cacheTime.Value > 0
                ? TimeSpan.FromMinutes(cacheTime.Value)
                : TimeSpan.FromMinutes(CachingDefaults.CacheTime);
            var cacheValue = HybridProvider.Get(key, acquirer, duration);
            if (cacheValue.HasValue)
                return cacheValue.Value;

            var result = acquirer();
            return result;
        }

        public virtual T GetSingerByKey<T>(string key)
        {
            var cacheValue = HybridProvider.Get<T>(key.ToUpper());

            return cacheValue.Value;
        }
     

        public virtual void SetEntity<T>(string key, T acquirer, int? cacheTime = null)
        {
            var duration = cacheTime.HasValue && cacheTime.Value > 0
                ? TimeSpan.FromMinutes(cacheTime.Value)
                : TimeSpan.FromMinutes(CachingDefaults.CacheTime);
             HybridProvider.Set(key, acquirer, duration);
        }

        public virtual bool GetByKey(string key)
        {
           return HybridProvider.Exists(key);
        }

        public virtual async Task<T> GetDbAsync<T>(string key, Func<Task<T>> acquirer, int? cacheTime = null)
        {
            var duration = cacheTime.HasValue && cacheTime.Value > 0
                ? TimeSpan.FromMinutes(cacheTime.Value)
                : TimeSpan.FromMinutes(CachingDefaults.CacheTime);
            var cacheValue = await HybridProvider.GetAsync(key, acquirer, duration);
            if (cacheValue.HasValue)
                return cacheValue.Value;

            var result = await acquirer();
            return result;
        }

        public void Start()
        {
            throw new NotImplementedException();
        }
    }
}
