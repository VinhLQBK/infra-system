﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InfraSystem.Common
{
    public static class ConstantDefine
    {
        public const string Province_Bac = "BAC";
        public const string Province_Nam = "NAM";

        public const string ORDER_TYPE_TRANSPORT = "TRANSPORT";
    }
}
